﻿## Overview


Sloth is a free and open-source application under Apache 2.0 License. 
This application is designed for all devices running Android 5.0 and newer. You may contribute to this project any time.

## Installation

Download source code, build and launch on your device. There is also **LeakCanary** included to detect memory leaks. You can simply delete it by unregistering from Manifest or deleting its class. If you do that please remember to delete gradle implementations.

You can also download latest stable release from Google Play: 
https://play.google.com/store/apps/details?id=dominik.kedziak.sloth_profilescheduler


## Permissions and its usages in the project


Bluetooth Services, need to change Bluetooth state: (BluetoothScheduler --> BluetoothServiceOn/Off)  
   

     <uses-permission android:name="android.permission.BLUETOOTH_ADMIN"/>
     <uses-permission android:name="android.permission.BLUETOOTH"/>

WiFi Services, need to change WiFi State (WiFiScheduler --> WiFiServiceOn/off)  
   

     <uses-permission android:name="android.permission.ACCESS_WIFI_STATE"/>
     <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE"/>
     <uses-permission android:name="android.permission.CHANGE_WIFI_STATE" />

Reschedule valid alarms after device is rebooted: (Tools --> onBootReceived)  

      <uses-permission android:name="android.permission.RECEIVE_BOOT_COMPLETED"/>

Disable battery optimizations in order to more exact alarms: (Asked in MainActivity.class)  

    <uses-permission android:name="android.permission.REQUEST_IGNORE_BATTERY_OPTIMIZATIONS"/>

Get notification policy permissions in order to set do not disturb mode (SoundModeScheduler --> SoundServiceDisturb)  
asked in ActivityEvents in SoundMode class
or via item menu in navigation drawer in MainActivity.class  

    <uses-permission android:name="android.permission.ACCESS_NOTIFICATION_POLICY"/>

Change screen brightness. Asked in ActivityEvents --> Brightness class or in MainActivity class from navigationdrawer menu  

    <uses-permission android:name="android.permission.WRITE_SETTINGS"  
      tools:ignore="ProtectedPermissions" />

Change account sync on/off (AccountSyncScheduler --> AccServiceOn/Off:

    <uses-permission android:name="android.permission.WRITE_SYNC_SETTINGS" />

Root permissions:  

    <!--REQUIRE ROOT ACCESS-->
    <uses-permission android:name="android.permission.WRITE_SETTINGS"
        tools:ignore="ProtectedPermissions" />
    <uses-permission android:name="android.permission.WRITE_SECURE_SETTINGS"
        tools:ignore="ProtectedPermissions" />
    
Asked in ActivityEvents --> PowerManagerReboot/DeviceOff classes, Tools --> RootUtils in order to reboot/ turn device off  



## SQLite3 database schema

This is a schema of database columns with its description. Primary database was accessed by Content Providers and later migrated to Room persistence library with a lot of changes, which are highlighted. 
Each dot contains as following:
*column name; data type; description*
  
 - _id Integer Primary Key Autoincerement  
  **Room version:**  Integer --> long
 - title - TEXT NOT NULL (e.g Turn WiFi on, alarm name, used to e.g reschedule alarm)  
 - date -  TEXT NOT NULL date of alarm. Decided to add a new column instead of combining date from columns below (year, month, day, hour, minute)  
                                    This way it should be more effective.  
 - year - TEXT NOT NULL year of set alarm  
    **Room version:** text --> integer 
 - month - TEXT NOT NULL month of alarm read from Calendar (NOTE!!: Calendar range is within 0-11 where 0 stands for January and 11 for December etc)  
 **Room version:** text --> int
 - day - TEXT NOT NULL day of set alarm  
  **Room version:** text --> int
 - hour - TEXT NOT NULL hourOfDay of set alarm (24H clock, there is migration to 12H clock where this option is set in preferences)  
  **Room version:** text --> int
 - minute - TEXT NOT NULL minute of set alarm  
  **Room version:** text --> int
 - repeat - TEXT NOT NULL is alarm repeating. Unnecessary made NOT NULL, that's why passing "" where it is single alarm.  
  **Room version:** text --> boolean, annotated with **@Nullable**
 - time - TEXT NOT NULL combinated values of hour and minute, including 0 if value was < 10 (to save as e.g 9:03 AM instead of 9:3 AM). Contains AM and PM if 12h clock mode  
 - intent - TEXT NOT NULL (**Removed in Room version**) Simple name of class, it was used before version 2.0 of app was released to open class where alarm was set with loaded data to it  
                                                e.g When turn WiFi on alarm was set, it opened class WiFi and loaded alarm data to it   
 - uri - TEXT NOT NULL (**Removed in Room version**) Uri passed to String and stored in database. It's been parsing to Uri to search for data etc. 
 - valid - TEXT NOT NULL can be True or False. Says if alarm is valid or not (turned on or off by the user)  
  **Room version:** text --> boolean
 - profile - TEXT NULLABLE, added since version 2.5.1 of app was released. Defines what profile of Sound is set by the user (SoundScheduler)  
 **Room version:** text --> int, **not null** now. Integers can't be null, passing 0 when defining in activities other than SoundScheduler.

**Changes in database since version 2.6.1 (Version Code 26):**
 - Brightness -  INTEGER NOT NULL, DEFAULT -1, this column is used when user defines custom brightness in Brightness activity. It contains its custom brightness value and is read when custom brightness alarm fires.

## SharedPreferences schema:


* "isFirstLaunch" - yet string, should be boolean. Can be true of false. If False launch intro and update value.  

**Since version 2.5 was released:**  
* "fontSize" - int of font size, can be changed from app preferences
"is24HClock" - boolean, whether user prefers to use 24h clock instead of 12h

**Since version 2.5.1 was released:**  

Sound Profiles:
 * "profile_1"
 * "profile_1_ring" --> ringtone volume (0;max), mute (-1), do not disturb (-2)
 * "profile_1_media" --> media volume (0; max)
 * "profile_1_alarms" --> alarm volume (0; max)  
 
    where "1" is variable int


## Alarm activity overview

You may have noticed that each of activities where user sets up an alarm contains only few lines of code and extend a custom activity instead of AppCompatActivity. There is a special class created which contains all functions and methods required by all activities which extend this class. This way code is not needlessly copied/pasted and is written just once! Each class, extending `BaseClass` calls appropriate methods, which are responsible for e.g hiding spinner, setting its source etc. After View functions are done, they all use the same `ViewModel`, but not all the activities use same fields.  Okay, that's fine, but how it determines which alarm should be set up? Let me show you a little diagram representing how it works.
![
](https://i.imgur.com/i0j15Q0.png)
Please note that, there is an error in algorithm run time in AlarmScheduler, should be constant time O(1).



First of all, activity calls Repository and sends there data to save.
Repository gets instance of Dao from database, calls Dao and sends data to save and waits for id of a freshly created row. When this id is received, it returns it back to the base class. After that, base class calls AlarmScheduler, sending Context, Title of alarm, calendar, especially timeInMilis and id of row in database casted to Uri. 
In AlarmScheduler class depending on a method which was selected, AlarmScheduler runs algorithm based on alarm title which alarm should be set up.  Algorithm runs in constant time O(1) as these are only if statements.  

