package dominik.kedziak.sloth_profilescheduler.SoundModeScheduler;

import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.util.Log;

import java.util.Objects;

import androidx.annotation.Nullable;
import dominik.kedziak.sloth_profilescheduler.Room.AlarmViewModel;
import dominik.kedziak.sloth_profilescheduler.Tools.NotificationHelper;

public class SoundServiceCustom extends IntentService {

    private static final String TAG = SoundServiceCustom.class.getSimpleName();


    public static PendingIntent getReminderPendingIntent(Context context, Uri uri) {
        Intent action = new Intent(context, SoundServiceCustom.class);
        action.setData(uri);
        return PendingIntent.getService(context, 0, action, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    public SoundServiceCustom() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        long id = 0;
        if (intent != null) {
            id = Long.parseLong(Objects.requireNonNull(intent.getData()).toString());
        }
        AlarmViewModel mViewModel = new AlarmViewModel(getApplication());
        boolean isValid = mViewModel.isAlarmValid(id);

        if (intent != null && intent.getData() != null) {

            if (isValid) {

                int profile = mViewModel.getProfile(id);
                AudioManager audioManager = (AudioManager) getBaseContext().getSystemService(Context.AUDIO_SERVICE);
                assert audioManager != null;
                String prof = "profile_";
                String ring = "_ring";
                String media = "_media";
                String alarms = "_alarms";
                String prefBuilder = prof + profile;
                String ringProf = prefBuilder + ring;
                String mediaProf = prefBuilder + media;
                String alarmProf = prefBuilder + alarms;
                //let's do our work
                SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
                int ringtoneVal = sp.getInt(ringProf, 3);
                int mediaVal = sp.getInt(mediaProf, 3);
                int alarmVal = sp.getInt(alarmProf, 3);


                audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, mediaVal, 0);
                audioManager.setStreamVolume(AudioManager.STREAM_ALARM, alarmVal, 2);

                Log.v(TAG, String.valueOf(ringtoneVal));
                if (ringtoneVal == -1) {
                    audioManager.setRingerMode(AudioManager.RINGER_MODE_VIBRATE);
                } else if (ringtoneVal == -2) {
                    audioManager.setStreamVolume(AudioManager.STREAM_RING, AudioManager.RINGER_MODE_SILENT, 4);
                } else {
                    audioManager.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
                    audioManager.setStreamVolume(AudioManager.STREAM_RING, ringtoneVal, AudioManager.FLAG_SHOW_UI +
                            AudioManager.FLAG_PLAY_SOUND);
                }
                NotificationHelper notificationHelper = new NotificationHelper();
                notificationHelper.createNotification(getApplicationContext(), id);
            } else {
                Log.v(TAG, "not valid");
            }

        }
    }
}
