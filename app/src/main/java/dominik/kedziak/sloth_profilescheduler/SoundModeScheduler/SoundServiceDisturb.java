package dominik.kedziak.sloth_profilescheduler.SoundModeScheduler;

import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.net.Uri;
import android.util.Log;

import java.util.Objects;

import androidx.annotation.Nullable;
import dominik.kedziak.sloth_profilescheduler.Room.AlarmDatabase;
import dominik.kedziak.sloth_profilescheduler.Room.AlarmViewModel;
import dominik.kedziak.sloth_profilescheduler.Room.AlarmsDao;
import dominik.kedziak.sloth_profilescheduler.Tools.NotificationHelper;



public class SoundServiceDisturb extends IntentService {
    private static final String TAG = SoundServiceDisturb.class.getSimpleName();


    public static PendingIntent getReminderPendingIntent(Context context, Uri uri) {
        Intent action = new Intent(context, SoundServiceDisturb.class);
        action.setData(uri);
        Log.v(TAG, String.valueOf(uri));
        Log.v(TAG, "uri passed into intent");
        String pi = String.valueOf(PendingIntent.getService(context, 0, action, PendingIntent.FLAG_UPDATE_CURRENT));
        Log.v(TAG, pi);
        return PendingIntent.getService(context, 0, action, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    public SoundServiceDisturb() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        long id = 0;
        if (intent != null) {
            id = Long.parseLong(Objects.requireNonNull(intent.getData()).toString());
        }
        AlarmViewModel mViewModel = new AlarmViewModel(getApplication());
        boolean isValid = mViewModel.isAlarmValid(id);

        if (intent != null && intent.getData() != null) {
            if (isValid) {

                final AudioManager audioManager = (AudioManager) getBaseContext().getSystemService(Context.AUDIO_SERVICE);
                assert audioManager != null;
                audioManager.setRingerMode(AudioManager.RINGER_MODE_SILENT);

                NotificationHelper notificationHelper = new NotificationHelper();
                notificationHelper.createNotification(getApplicationContext(), id);

            } else {
                Log.v(TAG, "not valid");
            }

        }
    }
}
