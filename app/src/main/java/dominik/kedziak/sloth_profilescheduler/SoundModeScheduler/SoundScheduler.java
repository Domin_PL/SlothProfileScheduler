package dominik.kedziak.sloth_profilescheduler.SoundModeScheduler;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import dominik.kedziak.sloth_profilescheduler.Tools.AlarmManagerProvider;

public class SoundScheduler {



    public void setAlarmLoud(Context context, long alarmTime, Uri reminderTask) {
        AlarmManager alarmManager = AlarmManagerProvider.getAlarmManager(context);
        Log.v("AlarmManager", "Initializing AM");
        PendingIntent operation =
                SoundServiceLoud.getReminderPendingIntent(context, reminderTask);

        if (Build.VERSION.SDK_INT >= 23) {
            assert alarmManager != null;
            alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, alarmTime, operation);
        } else {
            assert alarmManager != null;
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, alarmTime, operation);
        }
    }
    public void setAlarmMute(Context context, long alarmTime, Uri reminderTask) {
        AlarmManager alarmManager = AlarmManagerProvider.getAlarmManager(context);
        Log.v("AlarmManager", "Initializing AM");
        PendingIntent operation =
                SoundServiceMute.getReminderPendingIntent(context, reminderTask);

        if (Build.VERSION.SDK_INT >= 23) {
            assert alarmManager != null;
            alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, alarmTime, operation);
        } else {
            assert alarmManager != null;
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, alarmTime, operation);
        }
    }
    public void setAlarmDisturb(Context context, long alarmTime, Uri reminderTask) {
        AlarmManager alarmManager = AlarmManagerProvider.getAlarmManager(context);
        Log.v("AlarmManager", "Initializing AM");
        PendingIntent operation =
                SoundServiceDisturb.getReminderPendingIntent(context, reminderTask);

        if (Build.VERSION.SDK_INT >= 23) {
            assert alarmManager != null;
            alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, alarmTime, operation);
        } else {
            assert alarmManager != null;
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, alarmTime, operation);
        }
    }



    public void setAlarmLoudDaily(Context context, long timeInMillis, Uri reminderTask, long intervalDayInMilis) {
        AlarmManager alarmManager = AlarmManagerProvider.getAlarmManager(context);

        PendingIntent pendingIntent =
                SoundServiceLoud.getReminderPendingIntent(context, reminderTask);

        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, timeInMillis, intervalDayInMilis, pendingIntent);
    }

    public void setAlarmMuteDaily(Context context, long timeInMillis, Uri reminderTask, long intervalDayInMilis) {
        AlarmManager manager = AlarmManagerProvider.getAlarmManager(context);

        PendingIntent pendingIntent =
                SoundServiceMute.getReminderPendingIntent(context, reminderTask);

        manager.setRepeating(AlarmManager.RTC_WAKEUP,timeInMillis,intervalDayInMilis,pendingIntent);


    }
    public void setAlarmDisturbDaily(Context context, long timeInMillis, Uri reminderTask, long intervalDayInMilis) {
        AlarmManager manager = AlarmManagerProvider.getAlarmManager(context);

        PendingIntent pendingIntent =
                SoundServiceDisturb.getReminderPendingIntent(context, reminderTask);

        manager.setRepeating(AlarmManager.RTC_WAKEUP,timeInMillis,intervalDayInMilis,pendingIntent);


    }

    public void setAlarmCustomDaily(Context context, long timeInMillis, Uri reminderTask, long intervalDayInMilis) {
        AlarmManager manager = AlarmManagerProvider.getAlarmManager(context);

        PendingIntent pendingIntent =
                SoundServiceCustom.getReminderPendingIntent(context, reminderTask);

        manager.setRepeating(AlarmManager.RTC_WAKEUP,timeInMillis,intervalDayInMilis,pendingIntent);


    }

    public void setAlarmCustom(Context context, long alarmTime, Uri reminderTask) {
        AlarmManager alarmManager = AlarmManagerProvider.getAlarmManager(context);
        Log.v("AlarmManager", "Initializing AM");
        PendingIntent operation =
                SoundServiceCustom.getReminderPendingIntent(context, reminderTask);

        if (Build.VERSION.SDK_INT >= 23) {
            assert alarmManager != null;
            alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, alarmTime, operation);
        } else {
            assert alarmManager != null;
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, alarmTime, operation);
        }
    }
}
