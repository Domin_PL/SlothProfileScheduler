package dominik.kedziak.sloth_profilescheduler.Tools;

import android.app.AlarmManager;
import android.content.Context;
import android.util.Log;

public class AlarmManagerProvider {
    private static final String TAG = AlarmManagerProvider.class.getSimpleName();
    private static AlarmManager sAlarmManager;
    public static synchronized AlarmManager getAlarmManager(Context context) {
        if (sAlarmManager == null) {
            sAlarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            Log.v(TAG, "Getting calendar service");
        }
        return sAlarmManager;
    }
}
