package dominik.kedziak.sloth_profilescheduler.Tools

import android.content.Context
import android.content.Intent
import android.content.Intent.FLAG_ACTIVITY_NEW_TASK
import android.net.Uri
import android.os.Build
import android.provider.Settings
import androidx.annotation.RequiresApi

class Permissions {

    @RequiresApi(Build.VERSION_CODES.M)
    fun modifySettings(context: Context){
        if (!Settings.System.canWrite(context)){
            val intent = Intent()
            intent.action = Settings.ACTION_MANAGE_WRITE_SETTINGS
            intent.flags = FLAG_ACTIVITY_NEW_TASK
            intent.data = Uri.parse("package:" + context.packageName)
            context.startActivity(intent)
        }
    }

    fun isRootAvaible() {
        RootTools.isDeviceRooted()
    }

}