package dominik.kedziak.sloth_profilescheduler.Tools;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;

import androidx.lifecycle.ViewModelProviders;
import dominik.kedziak.sloth_profilescheduler.AccountSyncScheduler.AccSyncScheduler;
import dominik.kedziak.sloth_profilescheduler.BluetoothScheduler.BluetoothScheduler;
import dominik.kedziak.sloth_profilescheduler.BrightnessScheduler.BrightnessScheduler;
import dominik.kedziak.sloth_profilescheduler.PowerManagerScheduler.PowerManagerScheduler;
import dominik.kedziak.sloth_profilescheduler.R;
import dominik.kedziak.sloth_profilescheduler.Room.AlarmDatabase;
import dominik.kedziak.sloth_profilescheduler.Room.AlarmViewModel;
import dominik.kedziak.sloth_profilescheduler.Room.Alarms;
import dominik.kedziak.sloth_profilescheduler.Room.AlarmsDao;
import dominik.kedziak.sloth_profilescheduler.Room.Repository;
import dominik.kedziak.sloth_profilescheduler.SoundModeScheduler.SoundScheduler;
import dominik.kedziak.sloth_profilescheduler.WiFiScheduler.WiFiScheduler;

public class onBootReceived extends BroadcastReceiver {


    /**
     * This class is called when the device is rebooted. It is responsible for checking for
     * valid alarms, if there are any it must check if it should be fired in the past or not.
     * If it decides that it should be fired in the past and no longer repeated it must be disabled
     * @param context context provided by BroadcastReceiver
     * @param intent intent sent by BroadcastReceiver
     */

    @Override
    public void onReceive(final Context context, Intent intent) {
        if (Objects.equals(intent.getAction(), "android.intent.action.BOOT_COMPLETED")) {

            final long intervalDayInMilis = 86400000;

            final String TAG = onBootReceived.class.getSimpleName();
            // it must be performed on the background thread

            Application application = (Application) context.getApplicationContext();
            final Repository mRepository = new Repository(application);

            AsyncTask.execute(new Runnable() {
                @Override
                public void run() {

                    //run algorithm for every item in the database
                    List<Alarms> data = mRepository.getValidAlarms();
                    for (int i = 0; i < data.size(); i++){         // O(n)
                        Alarms alarms = data.get(i);

                        //Uri is id of row in database, just parsed into Uri format
                        Uri uri = Uri.parse(String.valueOf(alarms.get_id()));

                        //if alarm is valid check if it should be turned off or not
                        if (alarms.isValid()){        // O(1)
                            //check if it is repeating alarm or not
                            if (alarms.getRepeat() != null && !alarms.getRepeat().equals("")) { // O(1)
                                //alarm is repeating, we can set it up
                                //get calendar instance
                                Calendar calendar = Calendar.getInstance();
                                //set up calendar
                                calendar.set(alarms.getYear(), alarms.getMonth(),
                                        alarms.getDay(), alarms.getHour(), alarms.getMinute());
                                //Schedule repeating alarm depending on alarm title
                                String services = alarms.getTitle();
                                if (services.equals(context.getString(R.string.wifiOff))) {
                                    new WiFiScheduler().setAlarmOffDaily(context, calendar.getTimeInMillis(), uri,intervalDayInMilis);
                                } else if (services.equals(context.getString(R.string.wifiOn))) {
                                    new WiFiScheduler().setAlarmOnDaily(context, calendar.getTimeInMillis(), uri,intervalDayInMilis);
                                } else if (services.equals(context.getString(R.string.loudMode))) {
                                    new SoundScheduler().setAlarmLoudDaily(context, calendar.getTimeInMillis(), uri,intervalDayInMilis);
                                } else if (services.equals(context.getString(R.string.muteMode))) {
                                    new SoundScheduler().setAlarmMuteDaily(context, calendar.getTimeInMillis(), uri,intervalDayInMilis);
                                } else if (services.equals(context.getString(R.string.disturbMode))) {
                                    new SoundScheduler().setAlarmDisturbDaily(context, calendar.getTimeInMillis(), uri,intervalDayInMilis);
                                } else if (services.equals(context.getString(R.string.reboot))) {
                                    new PowerManagerScheduler().setAlarmRebootDaily(context, calendar.getTimeInMillis(), uri,intervalDayInMilis);
                                } else if (services.equals(context.getString(R.string.turnOff))) {
                                    new PowerManagerScheduler().setAlarmOffDaily(context, calendar.getTimeInMillis(), uri,intervalDayInMilis);
                                } else if (services.equals(context.getString(R.string.brightScreen))) {
                                    new BrightnessScheduler().setDailyAlarmBright(context, calendar.getTimeInMillis(), uri,intervalDayInMilis);
                                } else if (services.equals(context.getString(R.string.darkScreen))) {
                                    new BrightnessScheduler().setDailyAlarmDark(context, calendar.getTimeInMillis(), uri,intervalDayInMilis);
                                } else if (services.equals(context.getString(R.string.customScreen))) {
                                    new BrightnessScheduler().setDailyAlarmCustom(context, calendar.getTimeInMillis(), uri,intervalDayInMilis);
                                } else if (services.equals(context.getString(R.string.btOn))) {
                                    new BluetoothScheduler().setDailyAlarmOn(context, calendar.getTimeInMillis(), uri,intervalDayInMilis);
                                } else if (services.equals(context.getString(R.string.btOff))) {
                                    new BluetoothScheduler().setDailyAlarmOff(context, calendar.getTimeInMillis(), uri, intervalDayInMilis);
                                }else if (services.equals(context.getString(R.string.accSyncOff))) {
                                    new AccSyncScheduler().setDailyAlarmOff(context, calendar.getTimeInMillis(), uri, intervalDayInMilis);
                                } else if (services.equals(context.getString(R.string.accSyncOn))){
                                    new AccSyncScheduler().setDailyAlarmOn(context, calendar.getTimeInMillis(), uri, intervalDayInMilis);
                                } else if(services.contains(context.getString(R.string.profil))) {
                                    new SoundScheduler().setAlarmCustomDaily(context, calendar.getTimeInMillis(), uri,intervalDayInMilis);
                                }
                                else {
                                    Log.v(TAG, "no alarm found");
                                }


                            } else if (alarms.getRepeat() != null && alarms.getRepeat().equals("")){
                                //alarm is one time only, we must check if it should be reactivated,
                                //or just scheduled one more time
                                //get calendar instance
                                Calendar calendar = Calendar.getInstance();
                                //get variable instances of date and time
                                int year = calendar.get(Calendar.YEAR);
                                int month = calendar.get(Calendar.MONTH);
                                int day = calendar.get(Calendar.DAY_OF_MONTH);
                                int hour = calendar.get(Calendar.HOUR_OF_DAY);
                                int minute = calendar.get(Calendar.MINUTE);

                                //check if alarm was scheduled on the actual year or year in future
                                if (alarms.getYear() >= year){
                                    //check if alarm was scheduled in the same month or one in the future
                                    //also must take in care if the alarm was scheduled in the future year,
                                    //so that it will be always true
                                    //in every else condition we will disable alarm
                                     if (alarms.getMonth() >= month || alarms.getYear() > year){
                                         //check the day, same condition as above, next if statements
                                         //will work analogically
                                         if (alarms.getDay() >= day || alarms.getYear() > year){
                                             if (alarms.getHour() >= hour || alarms.getYear() > year){
                                                 if (alarms.getMinute() >= minute || alarms.getYear() > year){
                                                     //here we can set up alarm
                                                     calendar.set(alarms.getYear(), alarms.getMonth(),
                                                             alarms.getDay(), alarms.getHour(), alarms.getMinute());

                                                     //get title string
                                                     String services = alarms.getTitle();
                                                     if (services.equals(context.getString(R.string.wifiOff))) {
                                                         new WiFiScheduler().setAlarmOff(context, calendar.getTimeInMillis(), uri);
                                                     } else if (services.equals(context.getString(R.string.wifiOn))) {
                                                         new WiFiScheduler().setAlarmOn(context, calendar.getTimeInMillis(), uri);
                                                     } else if (services.equals(context.getString(R.string.loudMode))) {
                                                         new SoundScheduler().setAlarmLoud(context, calendar.getTimeInMillis(), uri);
                                                     } else if (services.equals(context.getString(R.string.muteMode))) {
                                                         new SoundScheduler().setAlarmMute(context, calendar.getTimeInMillis(), uri);
                                                     } else if (services.equals(context.getString(R.string.disturbMode))) {
                                                         new SoundScheduler().setAlarmDisturb(context, calendar.getTimeInMillis(), uri);
                                                     } else if (services.equals(context.getString(R.string.reboot))) {
                                                         new PowerManagerScheduler().setAlarmReboot(context, calendar.getTimeInMillis(), uri);
                                                     } else if (services.equals(context.getString(R.string.turnOff))) {
                                                         new PowerManagerScheduler().setAlarmOff(context, calendar.getTimeInMillis(), uri);
                                                     } else if (services.equals(context.getString(R.string.brightScreen))) {
                                                         new BrightnessScheduler().setAlarmBright(context, calendar.getTimeInMillis(), uri);
                                                     } else if (services.equals(context.getString(R.string.darkScreen))) {
                                                         new BrightnessScheduler().setAlarmDark(context, calendar.getTimeInMillis(), uri);
                                                     } else if (services.equals(context.getString(R.string.customScreen))) {
                                                         new BrightnessScheduler().setAlarmCustom(context, calendar.getTimeInMillis(), uri);
                                                     } else if (services.equals(context.getString(R.string.btOn))) {
                                                         new BluetoothScheduler().setAlarmOn(context, calendar.getTimeInMillis(), uri);
                                                     } else if (services.equals(context.getString(R.string.btOff))) {
                                                         new BluetoothScheduler().setAlarmOff(context, calendar.getTimeInMillis(), uri);
                                                     } else if (services.equals(context.getString(R.string.accSyncOff))) {
                                                         new AccSyncScheduler().setAlarmOff(context, calendar.getTimeInMillis(), uri);
                                                     } else if (services.equals(context.getString(R.string.accSyncOn))){
                                                         new AccSyncScheduler().setAlarmOn(context, calendar.getTimeInMillis(), uri);
                                                     } else if (services.contains(context.getString(R.string.profil))){
                                                         new SoundScheduler().setAlarmCustom(context, calendar.getTimeInMillis(), uri);
                                                     }
                                                     else {
                                                         Log.v(TAG, "no alarm found");
                                                     }
                                                 }else {
                                                     validationUpdater validationUpdater = new validationUpdater();
                                                     validationUpdater.disableAlarm(context, alarms.get_id());
                                                 }
                                             }
                                         }else {
                                             validationUpdater validationUpdater = new validationUpdater();
                                             validationUpdater.disableAlarm(context, alarms.get_id());
                                         }
                                    }else {
                                         validationUpdater validationUpdater = new validationUpdater();
                                         validationUpdater.disableAlarm(context, alarms.get_id());
                                     }
                                } else {
                                    validationUpdater validationUpdater = new validationUpdater();
                                    validationUpdater.disableAlarm(context, alarms.get_id());
                                }

                            }
                        }
                    }
                }
            });
        }
    }
}

