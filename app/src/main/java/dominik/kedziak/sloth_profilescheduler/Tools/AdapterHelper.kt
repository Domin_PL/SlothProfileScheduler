package dominik.kedziak.sloth_profilescheduler.Tools

import android.content.Context
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.CheckBox
import android.widget.ImageButton
import android.widget.TextView
import android.widget.ToggleButton
import dominik.kedziak.sloth_profilescheduler.R

class AdapterHelper {

    fun clickListeners(title: TextView, description: TextView, arrowDown: ImageButton, arrowUp: ImageButton,
                       extensionLayout: View, context: Context) {
        arrowDown.setOnClickListener { showLayout(arrowDown, arrowUp, extensionLayout, context)}
        arrowUp.setOnClickListener { showLayout(arrowDown, arrowUp, extensionLayout, context)}
        title.setOnClickListener { showLayout(arrowDown, arrowUp, extensionLayout, context)}
        description.setOnClickListener { showLayout(arrowDown, arrowUp, extensionLayout, context)}
    }

    fun showLayout(arrowDown: ImageButton, arrowUp: ImageButton,
                   extensionLayout: View, context: Context){

        if (arrowDown.visibility == View.VISIBLE &&
                arrowUp.visibility == View.GONE){
            arrowDown.visibility = View.GONE
            arrowUp.visibility = View.VISIBLE
            extensionLayout.visibility = View.VISIBLE
            val animation = AnimationUtils.loadAnimation(context, R.anim.slidedown_animation)
            extensionLayout.animation = animation
        } else if (arrowUp.visibility == View.VISIBLE &&
                arrowDown.visibility == View.GONE){
            arrowUp.visibility = View.GONE
            arrowDown.visibility = View.VISIBLE
            extensionLayout.visibility = View.GONE
            val animation = AnimationUtils.loadAnimation(context, R.anim.slidedown_animation)
            extensionLayout.animation = animation
        }
    }

    fun breakString(repeat: String, context: Context,
            monday: ToggleButton, tuesday: ToggleButton, wednesday: ToggleButton,
            thursday: ToggleButton, friday: ToggleButton, saturday: ToggleButton,
                              sunday: ToggleButton) {
        if (repeat.contains(context.getString(R.string.monday))) {
            monday.isChecked = true
        }
        if (repeat.contains(context.getString(R.string.tuesday))) {
            tuesday.isChecked = true

        }
        if (repeat.contains(context.getString(R.string.wednesday))) {
            wednesday.isChecked = true

        }
        if (repeat.contains(context.getString(R.string.thursday))) {
            thursday.isChecked = true

        }
        if (repeat.contains(context.getString(R.string.friday))) {
            friday.isChecked = true

        }
        if (repeat.contains(context.getString(R.string.saturday))) {
            saturday.isChecked = true

        }
        if (repeat.contains(context.getString(R.string.sunday))) {
            sunday.isChecked = true

        }
    }

    fun setOnRepeatCheckboxChangedListener(isRepeating: CheckBox,
                                           layout: androidx.constraintlayout.widget.ConstraintLayout){
        isRepeating.setOnCheckedChangeListener { _, b ->
            if (b) {
                layout.visibility = View.VISIBLE
                isRepeating.isChecked = true

            }
            if (!b){
                layout.visibility = View.GONE
            }
        }
    }

    fun deleteChar(s: String?): String? {
        return if (s == null || s.isEmpty())
            null
        else
            s.substring(0, s.length - 2)
    }


}