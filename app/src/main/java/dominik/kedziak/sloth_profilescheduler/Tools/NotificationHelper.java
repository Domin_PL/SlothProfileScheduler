package dominik.kedziak.sloth_profilescheduler.Tools;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.util.Calendar;

import androidx.core.app.NotificationCompat;
import dominik.kedziak.sloth_profilescheduler.R;

import static java.lang.Math.random;

public class NotificationHelper {


    public void createNotification(Context context, long id){
        if (new getDataRepeat().isOneRepeatAlarm(context, id)) {
            new validationUpdater().disableAlarm(context, id);
        }

        String NOTIFICATION = "isNotification";
        String IS24HCLOCK = "is24hClock";

        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        Boolean isNotification = sp.getBoolean(NOTIFICATION, true);
        if (isNotification) {
            Calendar calendar = Calendar.getInstance();
            int h = calendar.get(Calendar.HOUR_OF_DAY);
            int m = calendar.get(Calendar.MINUTE);
            int hourOfDay = h;
            String mTime;
            Boolean is24H = sp.getBoolean(IS24HCLOCK, false);

            if (!is24H && hourOfDay > 13 && hourOfDay != 24) {
                hourOfDay -= 12;
            }

            if (m < 10) {
                mTime = hourOfDay + ":" + "0" + m;
            } else {
                mTime = hourOfDay + ":" + m;
            }


            if (!is24H && h < 12) {
                mTime += " AM";
            } else if (!is24H) {
                mTime += " PM";
            }

            String title = new getDataRepeat().returnTitle(context, id);
            String description =  context.getString(R.string.fired) + " " + mTime;
            createNotification(context, title, description);
        }
    }

    private void createNotification(Context context, String title, String description) {

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        int notificationId = 1;
        String channelId = "channel_id";
        String channelName = "Channel Name";
        int importance = 0;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            importance = NotificationManager.IMPORTANCE_HIGH;
        }

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(
                    channelId, channelName, importance);
            notificationManager.createNotificationChannel(mChannel);
        }

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, channelId)
                .setSmallIcon(R.drawable.slothingo)
                .setContentTitle(title)
                .setContentText(description);


        notificationManager.notify(notificationId, mBuilder.build());
    }

}
