package dominik.kedziak.sloth_profilescheduler.Tools;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.util.Calendar;

import dominik.kedziak.sloth_profilescheduler.AccountSyncScheduler.AccSyncScheduler;
import dominik.kedziak.sloth_profilescheduler.BluetoothScheduler.BluetoothScheduler;
import dominik.kedziak.sloth_profilescheduler.BrightnessScheduler.BrightnessScheduler;
import dominik.kedziak.sloth_profilescheduler.PowerManagerScheduler.PowerManagerScheduler;
import dominik.kedziak.sloth_profilescheduler.R;
import dominik.kedziak.sloth_profilescheduler.Room.AlarmDatabase;
import dominik.kedziak.sloth_profilescheduler.Room.AlarmsDao;
import dominik.kedziak.sloth_profilescheduler.SoundModeScheduler.SoundScheduler;
import dominik.kedziak.sloth_profilescheduler.WiFiScheduler.WiFiScheduler;

public class adapterCalendarTools {

    public static final String TAG = adapterCalendarTools.class.getSimpleName();
    public long intervalDayInMilis = 86400000;


    /**
     * This method is responsible for updating alarm: updating values in database and in the end
     * rescheduling services
     * @param context context
     * @param id uniq id of row in database
     * @param services name of the alarm, so that we can figure out what service had been scheduled
     *                 e.g  turn wifi on for turning wifi on
     * @param calendar set up calendar
     * @param isRepeating is alarm one time only or just fires repeatly
     * @param repeatDescription description of the alarm
     */
    public void saveAlarm(final Context context, final long id, final String services,
                          Calendar calendar, boolean isRepeating, final String repeatDescription){

        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        String IS24HCLOCK = "is24hClock";
        Boolean is24H = sp.getBoolean(IS24HCLOCK, false);

        //get current year, month, day, hour, minute from calendar
        final int year = calendar.get(Calendar.YEAR);
        final int month = calendar.get(Calendar.MONTH);
        final int day = calendar.get(Calendar.DAY_OF_MONTH);
        final int hour = calendar.get(Calendar.HOUR_OF_DAY);
        final int minute = calendar.get(Calendar.MINUTE);


        //configire mTime string that is displayed as a date in recyclerview items
        String mTime;
        int hourOfDay = hour;
        if (!is24H && hourOfDay > 13 && hourOfDay != 24) {
            hourOfDay -= 12;
        }

        if (minute < 10) {
            mTime = hourOfDay + ":" + "0" + minute;
        } else {
            mTime = hourOfDay + ":" + minute;
        }


        if (!is24H && hour < 12){
            mTime +=" AM";
        } else if (!is24H){
            mTime += " PM";
        }

        int monthString = month + 1;
        Log.v(TAG, mTime);
        final String mDate;
        String mDay = String.valueOf(day);
        if (day < 10){
            mDay = 0 + String.valueOf(day);
        }
        if (month < 10) {
            mDate = mDay + "/0" + monthString + "/" + year;
        } else {
            mDate = mDay + "/" + monthString + "/" + year;
        }

        Log.v(TAG, mDate);

        //update values in database on the background thread
        final String finalTime = mTime;
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                AlarmDatabase db = AlarmDatabase.getInstance(context);
                AlarmsDao dao = db.alarmsDao();
                dao.updateAlarm(id, services, mDate, finalTime, year,
                        month, day, hour, minute, true, repeatDescription);
            }
        });


        Uri uri = Uri.parse(String.valueOf(id));

        /*
         * This is the most consuming algorithm. Check what kind of service it is and set proper alarm
         * depending on whether it is repeating alarm or not
         */
        if (isRepeating) {
            if (services.equals(context.getString(R.string.wifiOff))) {
                new WiFiScheduler().setAlarmOffDaily(context, calendar.getTimeInMillis(), uri, intervalDayInMilis);
            } else if (services.equals(context.getString(R.string.wifiOn))) {
                new WiFiScheduler().setAlarmOnDaily(context, calendar.getTimeInMillis(), uri, intervalDayInMilis);
            } else if (services.equals(context.getString(R.string.loudMode))) {
                new SoundScheduler().setAlarmLoudDaily(context, calendar.getTimeInMillis(), uri, intervalDayInMilis);
            } else if (services.equals(context.getString(R.string.muteMode))) {
                new SoundScheduler().setAlarmMuteDaily(context, calendar.getTimeInMillis(), uri, intervalDayInMilis);
            } else if (services.equals(context.getString(R.string.disturbMode))) {
                new SoundScheduler().setAlarmDisturbDaily(context, calendar.getTimeInMillis(), uri, intervalDayInMilis);
            } else if (services.equals(context.getString(R.string.reboot))) {
                new PowerManagerScheduler().setAlarmRebootDaily(context, calendar.getTimeInMillis(), uri, intervalDayInMilis);
            } else if (services.equals(context.getString(R.string.turnOff))) {
                new PowerManagerScheduler().setAlarmOffDaily(context, calendar.getTimeInMillis(), uri, intervalDayInMilis);
            } else if (services.equals(context.getString(R.string.brightScreen))) {
                new BrightnessScheduler().setDailyAlarmBright(context, calendar.getTimeInMillis(), uri, intervalDayInMilis);
            } else if (services.equals(context.getString(R.string.darkScreen))) {
                new BrightnessScheduler().setDailyAlarmDark(context, calendar.getTimeInMillis(), uri, intervalDayInMilis);
            } else if (services.equals(context.getString(R.string.customScreen))) {
                new BrightnessScheduler().setDailyAlarmCustom(context, calendar.getTimeInMillis(), uri, intervalDayInMilis);
            } else if (services.equals(context.getString(R.string.btOn))) {
                new BluetoothScheduler().setDailyAlarmOn(context, calendar.getTimeInMillis(), uri, intervalDayInMilis);
            } else if (services.equals(context.getString(R.string.btOff))) {
                new BluetoothScheduler().setDailyAlarmOff(context, calendar.getTimeInMillis(), uri, intervalDayInMilis);
            } else if (services.equals(context.getString(R.string.accSyncOff))) {
                new AccSyncScheduler().setDailyAlarmOff(context, calendar.getTimeInMillis(), uri, intervalDayInMilis);
            } else if (services.equals(context.getString(R.string.accSyncOn))) {
                new AccSyncScheduler().setDailyAlarmOn(context, calendar.getTimeInMillis(), uri, intervalDayInMilis);
            } else if (services.contains(context.getString(R.string.profil))){
                new SoundScheduler().setAlarmCustomDaily(context, calendar.getTimeInMillis(), uri, intervalDayInMilis);
            }
            else {
                Log.v(TAG, "no alarm found");
            }
        } else {
            if (services.equals(context.getString(R.string.wifiOff))) {
                new WiFiScheduler().setAlarmOff(context, calendar.getTimeInMillis(), uri);
            } else if (services.equals(context.getString(R.string.wifiOn))) {
                new WiFiScheduler().setAlarmOn(context, calendar.getTimeInMillis(), uri);
            } else if (services.equals(context.getString(R.string.loudMode))) {
                new SoundScheduler().setAlarmLoud(context, calendar.getTimeInMillis(), uri);
            } else if (services.equals(context.getString(R.string.muteMode))) {
                new SoundScheduler().setAlarmMute(context, calendar.getTimeInMillis(), uri);
            } else if (services.equals(context.getString(R.string.disturbMode))) {
                new SoundScheduler().setAlarmDisturb(context, calendar.getTimeInMillis(), uri);
            } else if (services.equals(context.getString(R.string.reboot))) {
                new PowerManagerScheduler().setAlarmReboot(context, calendar.getTimeInMillis(), uri);
            } else if (services.equals(context.getString(R.string.turnOff))) {
                new PowerManagerScheduler().setAlarmOff(context, calendar.getTimeInMillis(), uri);
            } else if (services.equals(context.getString(R.string.brightScreen))) {
                new BrightnessScheduler().setAlarmBright(context, calendar.getTimeInMillis(), uri);
            } else if (services.equals(context.getString(R.string.darkScreen))) {
                new BrightnessScheduler().setAlarmDark(context, calendar.getTimeInMillis(), uri);
            } else if (services.equals(context.getString(R.string.customScreen))) {
                new BrightnessScheduler().setAlarmCustom(context, calendar.getTimeInMillis(), uri);
            } else if (services.equals(context.getString(R.string.btOn))) {
                new BluetoothScheduler().setAlarmOn(context, calendar.getTimeInMillis(), uri);
            } else if (services.equals(context.getString(R.string.btOff))) {
                new BluetoothScheduler().setAlarmOff(context, calendar.getTimeInMillis(), uri);
            } else if (services.equals(context.getString(R.string.accSyncOff))) {
                new AccSyncScheduler().setAlarmOff(context, calendar.getTimeInMillis(), uri);
            } else if (services.equals(context.getString(R.string.accSyncOn))){
                new AccSyncScheduler().setAlarmOn(context, calendar.getTimeInMillis(), uri);
            } else if (services.contains(context.getString(R.string.profil))){
                new SoundScheduler().setAlarmCustom(context, calendar.getTimeInMillis(), uri);
            }

        Toast.makeText(context, context.getString(R.string.alarmUpdated), Toast.LENGTH_SHORT).show();
        }

    }


    /*
     *
     * @param context context
     * @param monday
     * @param tuesday
     * @param wednesday
     * @param thursday
     * @param friday
     * @param saturday
     * @param sunday
     * params above are ToggleButtons, by checking their state we want to create a "description" string
     * that will contain when alarm repeats
     * @return repeat string
     */

    public String releaseRepeat(Context context, final ToggleButton monday, final ToggleButton tuesday,
                                final ToggleButton wednesday, final ToggleButton thursday,
                                final ToggleButton friday, final ToggleButton saturday, final ToggleButton sunday) {
        String repeat = "";
        if (monday.isChecked()) {
            if (repeat.equals("")) {
                repeat = context.getString(R.string.repeat_on) + " " + context.getString(R.string.monday) + ", ";
            } else {
                repeat = repeat + context.getString(R.string.monday) + ", ";
            }
        }
        if (tuesday.isChecked()) {
            if (repeat.equals("")) {
                repeat = context.getString(R.string.repeat_on) + " " + context.getString(R.string.tuesday) + ", ";
            } else {
                repeat = repeat + context.getString(R.string.tuesday) + ", ";
            } }
        if (wednesday.isChecked()) {
            if (repeat.equals("")) {
                repeat = context.getString(R.string.repeat_on) + " " + context.getString(R.string.wednesday) + ", ";
            } else {
                repeat = repeat + context.getString(R.string.wednesday) + ", ";
            } }
        if (thursday.isChecked()) {
            if (repeat.equals("")) {
                repeat = context.getString(R.string.repeat_on) + " " + context.getString(R.string.thursday) + ", ";
            } else {
                repeat = repeat + context.getString(R.string.thursday) + ", ";
            } }
        if (friday.isChecked()) {
            if (repeat.equals("")) {
                repeat = context.getString(R.string.repeat_on) + " " + context.getString(R.string.friday) + ", ";
            } else {
                repeat = repeat + context.getString(R.string.friday) + ", ";
            } }
        if (saturday.isChecked()) {
            if (repeat.equals("")) {
                repeat = context.getString(R.string.repeat_on) + " " + context.getString(R.string.saturday) + ", ";
            } else {
                repeat = repeat + context.getString(R.string.saturday) + ", ";
            } }
        if (sunday.isChecked()) {
            if (repeat.equals("")) {
                repeat = context.getString(R.string.repeat_on) + " " + context.getString(R.string.sunday) + ", ";
            } else {
                repeat = repeat + context.getString(R.string.sunday) + ", ";
            } }

        if (monday.isChecked() && tuesday.isChecked() && wednesday.isChecked()
                && thursday.isChecked() && friday.isChecked() && saturday.isChecked() && sunday.isChecked()) {
            repeat = context.getString(R.string.repeat1Day);
        }
        Log.v("SM", repeat);
        return repeat;
    }



}
