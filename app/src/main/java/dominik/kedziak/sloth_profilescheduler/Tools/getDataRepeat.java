package dominik.kedziak.sloth_profilescheduler.Tools;

import android.content.Context;
import android.util.Log;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import dominik.kedziak.sloth_profilescheduler.R;
import dominik.kedziak.sloth_profilescheduler.Room.AlarmDatabase;
import dominik.kedziak.sloth_profilescheduler.Room.AlarmsDao;

public class getDataRepeat {

    private static final String TAG  = getDataRepeat.class.getSimpleName();

    /**
     * This method is responsible for returning repeat string, containing days,
     * on which alarm is scheduled. This is needed for other methods to check
     * if task is repeating or fires one time only.
     * @param context base context
     * @param id row of database
     * @return string containging days
     */
    private String returnRepeatString(Context context, Long id){
        AlarmDatabase db = AlarmDatabase.getInstance(context);
        AlarmsDao dao = db.alarmsDao();
        String repeat = dao.returnRepeatString(id);
        Log.v("String", repeat);
        return repeat;
    }

    /*
     * This method breaks string, checks what days of the weeks are contained in the String
     * and then adds equal Integers to the list. This is unnecessary to check if the
     * day is the day that alarm is to be fired. In short, algorithm checks actual date,
     * e.g if it is Monday, the code for it is 2. If the Monday (2) is contained in returned List,
     * alarm is valid for Monday.
     * @param repeat String that contains repeat days
     * @param context context
     * @return List of Integers that is assignable as following:
     *  sunday - 1
     *  monday - 2
     *  tuesday - 3
     *  up to saturday
     *  These Integers are equalivent to Android Documentations about week days
     */
    private List convertDataToList(String repeat, Context context){
        List <Integer> weekDays = new ArrayList<Integer>();

        String monday = context.getString(R.string.monday);
        String tuesday = context.getString(R.string.tuesday);
        String wednesday = context.getString(R.string.wednesday);
        String thursday = context.getString(R.string.thursday);
        String friday = context.getString(R.string.friday);
        String saturday = context.getString(R.string.saturday);
        String sunday = context.getString(R.string.sunday);

        Log.v("Strings", monday + ", "
                + tuesday + ", "
                + wednesday + ", "
                + thursday + ", "
                + friday + ", "
                + saturday + ", "
                + sunday + ", ");

        Log.v("repeat string", String.valueOf(repeat));

        if (repeat.contains(monday)){
            weekDays.add(2);
            Log.v("String checker", "Found monday");
        }
        if (repeat.contains(tuesday)){
            weekDays.add(3);
            Log.v("String checker", "Found tuesday");
        }
        if (repeat.contains(wednesday)){
            weekDays.add(4);
            Log.v("String checker", "Found wednesday");
        }
        if (repeat.contains(thursday)){
            weekDays.add(5);
            Log.v("String checker", "Found thursday");
        }
        if (repeat.contains(friday)){
            weekDays.add(6);
            Log.v("String checker", "Found friday");
        }
        if (repeat.contains(saturday)){
            weekDays.add(7);
            Log.v("String checker", "Found saturday");
        }
        if (repeat.contains(sunday)){
            weekDays.add(1);
            Log.v("String checker", "Found sunday");
        }

        Log.v("ListDays", String.valueOf(weekDays));
        return weekDays;
    }


    /**
     * This method is responsible for checking if the day alarm fired, is the day
     * that should be fired on. The action isn't performed yet, so that IntentService waits
     * for returning boolean from this method
     * @param context base context
     * @param id id of row in database
     * @return boolean detecting if alarm is valid or not
     */
    public boolean isTodayScheduled(Context context, Long id){
        Boolean isValid;
        Calendar calendar = Calendar.getInstance();
        //get current day
        int day = calendar.get(Calendar.DAY_OF_WEEK);
        //get return string that contains when alarm is to be fired
        String repeat = returnRepeatString(context, id);
        //convert this string to the List
        List valid = convertDataToList(repeat, context);
        Log.v("List", String.valueOf(valid));
        Log.v("repeat", String.valueOf(repeat));
        Log.v(TAG + "day", String.valueOf(day));
        //check if current day (its code) is contained in List
        isValid = valid.contains(day) || valid.isEmpty();
        return isValid;
    }

    /**
     * This method is responsible for checking if alarm is one time only. If is, method where it
     * had been called from disables alarm
     * @param context base context
     * @param id id of row in the database
     * @return boolean depending on alarm state
     */
    boolean isOneRepeatAlarm(Context context, Long id){
        Boolean isValid;
        //get string containing days of the week when alarm is to be fired
        String repeat = returnRepeatString(context, id);
        //convert this string to the list
        List valid = convertDataToList(repeat, context);
        Log.v("List", String.valueOf(valid));
        Log.v("repeat", String.valueOf(repeat));
        //if this list is empty and doesn't contain everyday string return true,
        // then alarm is one fire only (there is a special string made for everyday alarms)
        isValid = valid.isEmpty() && !repeat.contains(context.getString(R.string.repeat1Day));
        return isValid;
    }

    /**
     * This method returns title that is displayed in notification after task is done
     * @param context base context
     * @param id of row in database
     * @return title to display in notification
     */
    String returnTitle(Context context, long id){
        AlarmDatabase db = AlarmDatabase.getInstance(context);
        return db.alarmsDao().returnTitle(id);
    }

    /**
     * This method is only used in SoundServices to define what profile has user choose.
     * @param context base context
     * @param id id of row in database
     * @return choosen profile by the user
     */
    public int returnProfile(Context context, long id){
        AlarmDatabase db = AlarmDatabase.getInstance(context);
        int profile = db.alarmsDao().returnProfile(id);

        Log.v("String", String.valueOf(profile));
        return profile;
    }
}
