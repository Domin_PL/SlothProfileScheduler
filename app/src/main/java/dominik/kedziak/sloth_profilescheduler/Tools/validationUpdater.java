package dominik.kedziak.sloth_profilescheduler.Tools;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.Toast;

import org.jetbrains.annotations.NotNull;

import dominik.kedziak.sloth_profilescheduler.R;
import dominik.kedziak.sloth_profilescheduler.Room.AlarmDatabase;
import dominik.kedziak.sloth_profilescheduler.Room.AlarmsDao;

public class validationUpdater {

    private static final String TAG  = validationUpdater.class.getSimpleName();

    /**
     * This class is responsible for disabling/enabling alarms by setting proper boolean in database
     * @param switchActive switch responsible for disabling/enabling alarm
     * @param context app context
     * @param id row from database table
     */
    public void switchChangedListener(@NotNull final Switch switchActive, @NotNull final Context context, final long id) {
        switchActive.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(final CompoundButton compoundButton, final boolean b) {
                AsyncTask.execute(new Runnable() {
                    @Override
                    public void run() {
                        AlarmDatabase db = AlarmDatabase.getInstance(context);
                        AlarmsDao dao = db.alarmsDao();
                        dao.validAlarm(id, b);
                    }
                });
                if (!b) {
                    Toast.makeText(context, R.string.alarmDisabled, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(context, R.string.alarmEnabled, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void disableAlarm(final Context context,final long id){
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                AlarmDatabase db = AlarmDatabase.getInstance(context);
                db.alarmsDao().disableAlarm(id, false);
            }
        });
    }
}



