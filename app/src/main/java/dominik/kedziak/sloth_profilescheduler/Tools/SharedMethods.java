package dominik.kedziak.sloth_profilescheduler.Tools;

import android.content.Context;
import android.view.View;
import android.widget.Switch;
import android.widget.ToggleButton;

import androidx.constraintlayout.widget.ConstraintLayout;
import dominik.kedziak.sloth_profilescheduler.R;


public class SharedMethods {


    public void setRepeatChanged(Switch repeat, ConstraintLayout repeatLine) {
        if (repeat.isChecked()) {
            repeat.setChecked(false);
            repeatLine.setVisibility(View.GONE);
        } else {
            repeat.setChecked(true);
            repeatLine.setVisibility(View.VISIBLE);
        }
    }

    public void onRepeatChanged(Switch repeat, ConstraintLayout repeatLine) {
        if (repeat.isChecked()) {
            repeatLine.setVisibility(View.VISIBLE);
        } else {
            repeatLine.setVisibility(View.GONE);
        }
    }

    public String releaseRepeat(Context mContext,  ToggleButton monday,
                                ToggleButton tuesday, ToggleButton wednesday,
                                ToggleButton thursday, ToggleButton friday,
                                ToggleButton saturday, ToggleButton sunday) {

        StringBuilder builder = new StringBuilder();
        builder.append(mContext.getString(R.string.repeat_on)).append(" ");

        if (monday.isChecked() && tuesday.isChecked() && wednesday.isChecked()
                && thursday.isChecked() && friday.isChecked() && saturday.isChecked()
                && sunday.isChecked()){
            builder.setLength(0);
            builder = new StringBuilder();
            builder.append(mContext.getString(R.string.repeat1Day));
            return builder.toString();
        }

        String semicolon = ", ";

        if (monday.isChecked()) builder.append(mContext.getString(R.string.monday)).append(semicolon);
        if (tuesday.isChecked()) builder.append(mContext.getString(R.string.tuesday)).append(semicolon);
        if (wednesday.isChecked()) builder.append(mContext.getString(R.string.wednesday)).append(semicolon);
        if (thursday.isChecked()) builder.append(mContext.getString(R.string.thursday)).append(semicolon);
        if (friday.isChecked()) builder.append(mContext.getString(R.string.friday)).append(semicolon);
        if (saturday.isChecked()) builder.append(mContext.getString(R.string.saturday)).append(semicolon);
        if (sunday.isChecked()) builder.append(mContext.getString(R.string.sunday));

        int length = builder.length() - 2;
        char lastChar = builder.charAt(length);
        if (lastChar == ',') builder.setLength(length);

        return builder.toString();
    }

}