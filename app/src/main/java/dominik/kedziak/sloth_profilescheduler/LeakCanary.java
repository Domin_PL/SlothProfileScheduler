package dominik.kedziak.sloth_profilescheduler;

import android.app.Application;

public class LeakCanary extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        if (com.squareup.leakcanary.LeakCanary.isInAnalyzerProcess(this)){
            return;
        }
        com.squareup.leakcanary.LeakCanary.install(this);
    }
}
