package dominik.kedziak.sloth_profilescheduler.Preferences

import android.annotation.SuppressLint
import android.content.SharedPreferences
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.View
import android.widget.CompoundButton
import android.widget.SeekBar
import android.widget.Switch
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import dominik.kedziak.sloth_profilescheduler.R


class Preferences : AppCompatActivity(){

    lateinit var fontSizeSeekbar: SeekBar
    lateinit var notificationSwitch: Switch
    lateinit var clockSwitch: Switch
    lateinit var sp: SharedPreferences
    lateinit var editor: SharedPreferences.Editor
    lateinit var pxSizeTextView: TextView
    lateinit var sampleTextView: TextView
    var is24H = false
    var isNotification = true
    var fontSize = 14
    var NOTIFICATION = "isNotification"
    var IS24HCLOCK = "is24hClock"
    var FONTSIZE = "fontSize"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_preferences)
        title = applicationContext.getString(R.string.preferences)
        sp = PreferenceManager.getDefaultSharedPreferences(this)
        editor = sp.edit()
        initializeViews()
    }

    fun initializeViews(){
        fontSizeSeekbar = findViewById(R.id.fontSizeSeekbar)
        notificationSwitch = findViewById(R.id.notificationSwitch)
        clockSwitch = findViewById(R.id.is24Switch)
        pxSizeTextView = findViewById(R.id.fontPxs)
        sampleTextView = findViewById(R.id.sampleText)

        is24H = sp.getBoolean(IS24HCLOCK, false)
        isNotification = sp.getBoolean(NOTIFICATION, true)

        notificationSwitch.isChecked = isNotification
        clockSwitch.isChecked = is24H

        fontSize = sp.getInt(FONTSIZE, 15)
        sampleTextView.textSize = fontSize.toFloat()
        pxSizeTextView.text = fontSize.toString() + " " + "sp"
        fontSizeSeekbar.apply{
            max = 30
            fontSizeSeekbar.progress = fontSize
        }
        seekbarOnChangedListener()
        switchOnCheckedChangedListeners()
    }

    private fun switchOnCheckedChangedListeners() {
        notificationSwitch.setOnCheckedChangeListener(object : CompoundButton.OnCheckedChangeListener {
            override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
                    editor.apply{
                        remove(NOTIFICATION)
                        putBoolean(NOTIFICATION, isChecked)
                        commit()
                    }

            }
        })

        clockSwitch.setOnCheckedChangeListener(object : CompoundButton.OnCheckedChangeListener{
            override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
                editor.apply{
                    remove(IS24HCLOCK)
                    putBoolean(IS24HCLOCK, isChecked)
                    commit()
                }
            }
        })
    }

    private fun seekbarOnChangedListener() {
        fontSizeSeekbar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener{
            @SuppressLint("SetTextI18n")
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                sampleTextView.textSize = progress.toFloat()
                pxSizeTextView.text = "$progress sp"
                editor.apply{
                    remove(FONTSIZE)
                    putInt(FONTSIZE, progress)
                    commit()
                }
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {
            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
            }
        })
    }

    @SuppressLint("CommitPrefEdits")
    fun NotificationLine() {
        if (notificationSwitch.isChecked){
            notificationSwitch.isChecked = false
            editor.apply {
                remove(NOTIFICATION)
                putBoolean(NOTIFICATION, false)
                commit()
            }
        } else {
            notificationSwitch.isChecked = true
            editor.apply {
                remove(NOTIFICATION)
                putBoolean(NOTIFICATION, true)
                commit()
            }
        }
    }

    fun is24hLine(view: View){
        if (clockSwitch.isChecked){
            clockSwitch.isChecked = false
            editor.apply{
                remove(IS24HCLOCK)
                putBoolean(IS24HCLOCK, false)
                commit()
            }

        } else {
            clockSwitch.isChecked = true
            editor.apply{
                remove(IS24HCLOCK)
                putBoolean(IS24HCLOCK, true)
                commit()
            }
        }
    }



}

