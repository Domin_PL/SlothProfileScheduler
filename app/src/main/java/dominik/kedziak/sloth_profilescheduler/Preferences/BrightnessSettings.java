package dominik.kedziak.sloth_profilescheduler.Preferences;

import android.content.ContentResolver;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.Switch;

import dominik.kedziak.sloth_profilescheduler.R;

public class BrightnessSettings extends AppCompatActivity{

    private SeekBar brightSeekbar, darkSeekbar;
    private Switch brightSwitch, darkSwitch;
    private int darkValue, brightValue;
    private ContentResolver contentResolver;
    private SharedPreferences mPreferences;
    private SharedPreferences.Editor mEditor;
    private Boolean isCheckedBright, isCheckedDark;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        setTitle(R.string.settings);
        initializeViews();
        boolean canWrite = android.provider.Settings.System.canWrite(getApplicationContext());
        if (!canWrite) {
            getPermission();
        }
        darkSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {
                darkValue = progress;
                if (isCheckedDark){
                    android.provider.Settings.System.putInt(contentResolver, android.provider.Settings.System.SCREEN_BRIGHTNESS, progress);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        brightSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {
                brightValue = progress;
                if (isCheckedBright){
                    android.provider.Settings.System.putInt(contentResolver, android.provider.Settings.System.SCREEN_BRIGHTNESS, progress);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    private void initializeViews(){
        brightSeekbar = findViewById(R.id.bright_seekBar);
        darkSeekbar = findViewById(R.id.dark_seekBar);
        brightSwitch = findViewById(R.id.bright_preview);
        darkSwitch = findViewById(R.id.dark_preview);
        darkSeekbar.setMax(255);
        brightSeekbar.setMax(255);
        contentResolver = getContentResolver();
        mPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String valDark = mPreferences.getString("brightness_dark", "50");
        String valBright = mPreferences.getString("brightness_bright", "225");
        int valD = Integer.parseInt(valDark);
        int valB = Integer.parseInt(valBright);
        brightSeekbar.setProgress(valB);
        darkSeekbar.setProgress(valD);
        darkSwitch.setChecked(false);
        brightSwitch.setChecked(false);
        isCheckedBright = false;
        isCheckedDark = false;
        darkSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                isCheckedDark = isChecked;
            }
        });
        brightSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                isCheckedBright = isChecked;
            }
        });
    }

    public void getPermission(){
        Intent intent = new Intent(android.provider.Settings.ACTION_MANAGE_WRITE_SETTINGS);
        intent.setData(Uri.parse("package:" + getApplicationContext().getPackageName()));
        startActivity(intent);
    }
    private void save(){
        mPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        mEditor = mPreferences.edit();
        mEditor.remove("brightness_bright");
        mEditor.putString("brightness_bright", String.valueOf(brightValue));
        mEditor.commit();
        mEditor = mPreferences.edit();
        mEditor.remove("brightness_dark");
        mEditor.putString("brightness_dark", String.valueOf(darkValue));
        mEditor.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        MenuItem menuItem = menu.findItem(R.id.dismiss);
            menuItem.setVisible(false);
        return true;
    }

    @Override
    public void invalidateOptionsMenu() {
        super.invalidateOptionsMenu();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.tick:
                save();
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
