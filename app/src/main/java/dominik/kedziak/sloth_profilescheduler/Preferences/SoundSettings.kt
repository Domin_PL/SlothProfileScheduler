package dominik.kedziak.sloth_profilescheduler.Preferences

import android.content.Context
import android.media.AudioManager
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import dominik.kedziak.sloth_profilescheduler.R

class SoundSettings : AppCompatActivity(){

    lateinit var ringtoneSeekbar: SeekBar
    lateinit var mediaSeekbar: SeekBar
    lateinit var alarmSeekBar: SeekBar
    lateinit var profileSpinner: Spinner
    lateinit var ringtoneValueText: TextView
    lateinit var ringtoneModeSpinner: Spinner
    /*
     *Constant fields, see schema in dataLoader()
     */
    val prof = "profile_"
    val ring = "_ring"
    val media = "_media"
    val alarms = "_alarms"

    //global variables
    var ringtoneValue = 3
    var mediaValue = 3
    var alarmValue = 3

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sound_settings)

        title = getString(R.string.sound_prefs)

        //let's initialize ringtone textview showing seekbar's progress
        // - this must be invisible when phone is to be muted or set into do not disturb spinnerMode

        ringtoneValueText = findViewById(R.id.ringtoneValueText)

        //hence we load first profil as main, we pass 0, it won't cause any errors now
        dataLoader(0)
        initializeSpinners()
        initializeSeekbars()
        reinitializeSpinner(ringtoneValue)


    }


    fun reloadData(profile: Int){
        dataLoader(profile)
        initializeSeekbars()
        reinitializeSpinner(ringtoneValue)

    }

    fun reinitializeSpinner(value: Int){
        if (value == -1) {
            ringtoneModeSpinner.setSelection(1)
        } else if (value == -2){
            ringtoneModeSpinner.setSelection(2)
        } else {
            ringtoneModeSpinner.setSelection(0)
        }
    }

    fun initializeSpinners(){
        val seekBarLayout: ConstraintLayout = findViewById(R.id.seekbarLayout)
        profileSpinner = findViewById(R.id.profileSpinner)
        val adapter = ArrayAdapter.createFromResource(this,
                R.array.profile_spinner, android.R.layout.simple_spinner_item)
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        profileSpinner.adapter = adapter

        ringtoneModeSpinner = findViewById(R.id.ringtoneModeSpinner)
        val ringtoneAdapter = ArrayAdapter.createFromResource(this,
                R.array.ringtone_mode_spinner, android.R.layout.simple_spinner_item)
        ringtoneAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        ringtoneModeSpinner.adapter = ringtoneAdapter


        //onChangedListeners
        profileSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                reloadData(position)
            }
        }

        ringtoneModeSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                if (position == 0) {
                    seekBarLayout.visibility = View.VISIBLE
                    ringtoneValueText.visibility = View.VISIBLE
                } else if (position == 1){
                    ringtoneValue = -1
                    seekBarLayout.visibility = View.GONE
                    ringtoneValueText.visibility = View.INVISIBLE
                } else if (position == 2){
                    ringtoneValue = -2
                    seekBarLayout.visibility = View.GONE
                    ringtoneValueText.visibility = View.INVISIBLE
                }
            }

        }
    }

    fun initializeSeekbars(){
        ringtoneSeekbar = findViewById(R.id.ringtoneSeekbar)
        mediaSeekbar = findViewById(R.id.mediaSeekbar)
        alarmSeekBar = findViewById(R.id.alarmSeekbar)

        //let's get max values from AlarmManager class
        val am = applicationContext.getSystemService(Context.AUDIO_SERVICE) as AudioManager
        val ringtoneMax = am.getStreamMaxVolume(AudioManager.STREAM_RING)
        val musicMax = am.getStreamMaxVolume(AudioManager.STREAM_MUSIC)
        val alarmMax = am.getStreamMaxVolume(AudioManager.STREAM_ALARM)


        //let's set up those values to the seekbar
        ringtoneSeekbar.max = ringtoneMax
        mediaSeekbar.max = musicMax
        alarmSeekBar.max = alarmMax

        //let's set up textviews showing up its progress
        val mediaText = findViewById<TextView>(R.id.mediaValueText)
        val alarmText: TextView = findViewById(R.id.alarmValueText)

        //let's set default/previous saved values on seekbar and textviews
        if (ringtoneValue != -1 && ringtoneValue != -2) {
            ringtoneSeekbar.progress = ringtoneValue
            ringtoneValueText.text = ringtoneValue.toString()
        }
        mediaSeekbar.progress = mediaValue
        mediaText.text = mediaValue.toString()
        alarmSeekBar.progress = alarmValue
        alarmText.text = alarmValue.toString()

        //onProgressChanged listeners

        ringtoneSeekbar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener{
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                ringtoneValueText.text = progress.toString()
                ringtoneValue = progress
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {
            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
            }
        })

        mediaSeekbar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener{
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                mediaText.text = progress.toString()
                mediaValue = progress
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {
            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
            }
        })

        alarmSeekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener{
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                alarmText.text = progress.toString()
                alarmValue = progress
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {
            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
            }
        })
    }



    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.save_changes, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item!!.itemId) {
            R.id.save_changes -> {
                save()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    fun save(){
        // we need to define which profile is currently running
        val profile = profileSpinner.selectedItemPosition
        val preferences = PreferenceManager.getDefaultSharedPreferences(this)
        val prefBuilder = prof + profile
        val ringProf = prefBuilder + ring
        val mediaProf = prefBuilder + media
        val alarmProf = prefBuilder + alarms
        val editor = preferences.edit()
        editor.remove(ringProf)
        editor.remove(mediaProf)
        editor.remove(alarmProf)
        editor.putInt(ringProf, ringtoneValue)
        editor.putInt(mediaProf, mediaValue)
        editor.putInt(alarmProf, alarmValue)
        val hasSucced = editor.commit()
        if (hasSucced) {
            Toast.makeText(this, R.string.settings_updated, Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(this, R.string.settings_fail, Toast.LENGTH_SHORT).show()
        }


    }

    fun dataLoader(profile: Int){
        /**
         * Schema of SharedPreferences SoundPreferences:
         * "profile_1"
         * "profile_1_ring" --> variable (0;max), mute (-1), do not disturb (-2)
         * "profile_1_media"
         * "profile_1_alarms"
         * where "1" is variable int
         */

        val preferences = PreferenceManager.getDefaultSharedPreferences(this)
        val prefBuilder = prof + profile
        val ringProf = prefBuilder + ring
        val mediaProf = prefBuilder + media
        val alarmProf = prefBuilder + alarms

        //safe default values
        ringtoneValue = preferences.getInt(ringProf, 3)
        mediaValue = preferences.getInt(mediaProf, 3)
        alarmValue = preferences.getInt(alarmProf, 3)



    }
}