package dominik.kedziak.sloth_profilescheduler.BrightnessScheduler;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import java.util.Arrays;
import java.util.Collections;

import dominik.kedziak.sloth_profilescheduler.Tools.AlarmManagerProvider;

public class BrightnessScheduler {


    public void setAlarmBright(Context context, long alarmTime, Uri reminderTask) {
        AlarmManager alarmManager = AlarmManagerProvider.getAlarmManager(context);
        PendingIntent operation =
                BrightnessServiceBright.getReminderPendingIntent(context, reminderTask);

        if (Build.VERSION.SDK_INT >= 23) {
            assert alarmManager != null;
            alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, alarmTime, operation);
        } else {
            assert alarmManager != null;
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, alarmTime, operation);
        }

    }

    public void setAlarmDark(Context context, long alarmTime, Uri reminderTask) {
        AlarmManager alarmManager = AlarmManagerProvider.getAlarmManager(context);
        PendingIntent operation =
                BrightnessServiceDark.getReminderPendingIntent(context, reminderTask);

        if (Build.VERSION.SDK_INT >= 23) {
            assert alarmManager != null;
            alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, alarmTime, operation);
        } else {
            assert alarmManager != null;
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, alarmTime, operation);
        }
    }
    public void setAlarmCustom(Context context, long alarmTime, Uri reminderTask) {
        AlarmManager alarmManager = AlarmManagerProvider.getAlarmManager(context);
        Log.v("AlarmManager", "Initializing AM");
        PendingIntent operation =
                BrightnessServiceCustom.getReminderPendingIntent(context, reminderTask);

        if (Build.VERSION.SDK_INT >= 23) {
            assert alarmManager != null;
            alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, alarmTime, operation);
        } else {
            assert alarmManager != null;
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, alarmTime, operation);
        }
    }


    public void setDailyAlarmBright(Context context, long alarmTime, Uri reminderTask, long repeatTime) {
        AlarmManager manager = AlarmManagerProvider.getAlarmManager(context);

        PendingIntent operation =
                BrightnessServiceBright.getReminderPendingIntent(context, reminderTask);

        if (Build.VERSION.SDK_INT >= 23) {

            manager.setRepeating(AlarmManager.RTC_WAKEUP, alarmTime, repeatTime, operation);
        } else {
            manager.setRepeating(AlarmManager.RTC_WAKEUP, alarmTime,  repeatTime, operation);
        }

    }
    public void setDailyAlarmDark(Context context, long alarmTime, Uri reminderTask, long repeatTime) {
        AlarmManager manager = AlarmManagerProvider.getAlarmManager(context);

        PendingIntent operation =
                BrightnessServiceDark.getReminderPendingIntent(context, reminderTask);

        if (Build.VERSION.SDK_INT >= 23) {

            manager.setRepeating(AlarmManager.RTC_WAKEUP, alarmTime, repeatTime, operation);
        } else {
            manager.setRepeating(AlarmManager.RTC_WAKEUP, alarmTime,  repeatTime, operation);
        }

    }
    public void setDailyAlarmCustom(Context context, long alarmTime, Uri reminderTask, long repeatTime) {
        AlarmManager manager = AlarmManagerProvider.getAlarmManager(context);

        PendingIntent operation =
                BrightnessServiceCustom.getReminderPendingIntent(context, reminderTask);

        if (Build.VERSION.SDK_INT >= 23) {

            manager.setRepeating(AlarmManager.RTC_WAKEUP, alarmTime, repeatTime, operation);
        } else {
            manager.setRepeating(AlarmManager.RTC_WAKEUP, alarmTime,  repeatTime, operation);
        }

    }

}
