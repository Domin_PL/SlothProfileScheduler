package dominik.kedziak.sloth_profilescheduler.BrightnessScheduler;

import android.app.IntentService;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.util.Log;

import java.util.Objects;

import androidx.annotation.Nullable;
import dominik.kedziak.sloth_profilescheduler.Room.AlarmDatabase;
import dominik.kedziak.sloth_profilescheduler.Room.AlarmViewModel;
import dominik.kedziak.sloth_profilescheduler.Room.AlarmsDao;
import dominik.kedziak.sloth_profilescheduler.Room.Repository;
import dominik.kedziak.sloth_profilescheduler.Tools.NotificationHelper;


public class BrightnessServiceCustom extends IntentService {

    private static final String TAG = BrightnessServiceCustom.class.getSimpleName();


    public static PendingIntent getReminderPendingIntent(Context context, Uri uri) {
        Intent action = new Intent(context, BrightnessServiceCustom.class);
        action.setData(uri);
        return PendingIntent.getService(context, 0, action, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    public BrightnessServiceCustom() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        long id = 0;
        if (intent != null) {
            id = Long.parseLong(Objects.requireNonNull(intent.getData()).toString());
        }


        AlarmViewModel mViewModel = new AlarmViewModel(getApplication());
        boolean isValid = mViewModel.isAlarmValid(id);


        if (intent != null && intent.getData() != null) {
            if (isValid) {
                ContentResolver contentResolver = getContentResolver();
                int value = mViewModel.getCustomBrightnessValue(id);
                Settings.System.putInt(contentResolver, Settings.System.SCREEN_BRIGHTNESS, value);
                Log.v(TAG, "Turning custom screen value");
                NotificationHelper notificationHelper = new NotificationHelper();
                notificationHelper.createNotification(getApplicationContext(), id);
            } else {
                Log.v(TAG, "not valid");
            }
        }
    }
}
