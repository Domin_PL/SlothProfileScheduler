package dominik.kedziak.sloth_profilescheduler.BrightnessScheduler;

import android.app.IntentService;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.util.Log;

import java.util.Objects;

import androidx.annotation.Nullable;
import dominik.kedziak.sloth_profilescheduler.Room.AlarmDatabase;
import dominik.kedziak.sloth_profilescheduler.Room.AlarmViewModel;
import dominik.kedziak.sloth_profilescheduler.Room.AlarmsDao;
import dominik.kedziak.sloth_profilescheduler.Tools.NotificationHelper;



public class BrightnessServiceBright extends IntentService {
    private static final String TAG = BrightnessServiceBright.class.getSimpleName();


    public static PendingIntent getReminderPendingIntent(Context context, Uri uri) {
        Intent action = new Intent(context, BrightnessServiceBright.class);
        action.setData(uri);
        return PendingIntent.getService(context, 0, action, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    public BrightnessServiceBright() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        long id = 0;
        if (intent != null) {
            id = Long.parseLong(Objects.requireNonNull(intent.getData()).toString());
        }

        AlarmViewModel mViewModel = new AlarmViewModel(getApplication());
        boolean isValid = mViewModel.isAlarmValid(id);


        if (intent != null && intent.getData() != null) {

            if (isValid) {
                Log.v(TAG, "alarm is valid!");
                SharedPreferences mPreferences;
                ContentResolver contentResolver;
                contentResolver = getContentResolver();
                mPreferences = PreferenceManager.getDefaultSharedPreferences(this);
                String value = mPreferences.getString("brightness_bright", "255");
                int val = 0;
                if (value != null) {
                    val = Integer.parseInt(value);
                }

                // we don't have to check if we have write settings permissions, hence application
                // deosn't allow user to schedule brightness task unless he gives write settings permissions
                Settings.System.putInt(contentResolver, Settings.System.SCREEN_BRIGHTNESS, val);

              NotificationHelper notificationHelper = new NotificationHelper();
              notificationHelper.createNotification(getApplicationContext(), id);
            } else {
                Log.v(TAG, "not valid");
            }
        }
    }
}
