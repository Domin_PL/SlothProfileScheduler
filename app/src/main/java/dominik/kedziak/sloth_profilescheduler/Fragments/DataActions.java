package dominik.kedziak.sloth_profilescheduler.Fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import dominik.kedziak.sloth_profilescheduler.ActivityEvents.AccSync;
import dominik.kedziak.sloth_profilescheduler.ActivityEvents.Bluetooth;
import dominik.kedziak.sloth_profilescheduler.ActivityEvents.Brightness;
import dominik.kedziak.sloth_profilescheduler.ActivityEvents.PowerManagerDeviceOff;
import dominik.kedziak.sloth_profilescheduler.ActivityEvents.PowerManagerReboot;
import dominik.kedziak.sloth_profilescheduler.ActivityEvents.SoundMode;
import dominik.kedziak.sloth_profilescheduler.ActivityEvents.WiFi;
import dominik.kedziak.sloth_profilescheduler.Adapters.rvAdapter;
import dominik.kedziak.sloth_profilescheduler.R;

public class DataActions extends Fragment implements rvAdapter.ItemClickListener {

    private Context context;

    public DataActions() {
        // empty constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        this.context = null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.context = null;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_data_actions, container, false);

        //         data to populate the RecyclerView with
        String[] acts = getResources().getStringArray(R.array.dataActions);
        List<String> acts2 =  Arrays.asList(acts);
        List<String> Actions = new ArrayList<String>(acts2);


        String[] desc = getResources().getStringArray(R.array.descriptions);
        List<String> desc2 = Arrays.asList(desc);
        List<String> Descriptions = new ArrayList<String>(desc2);

        // set up the RecyclerView
        RecyclerView recyclerView = view.findViewById(R.id.recyclerview_main);
        LinearLayoutManager linearLayoutManager =
                new LinearLayoutManager(context,
                        RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        rvAdapter adapter = new rvAdapter(context,
                Actions, Descriptions);
        adapter.setClickListener(this);
        recyclerView.setAdapter(adapter);

        return view;
    }

    @Override
    public void onItemClick(View view, int position) {
        switch (position) {
            case 0:
                Intent intent = new Intent(getActivity(), WiFi.class);
                startActivity(intent);
                break;
            case 1:
                Intent intent1 = new Intent(getActivity(), Bluetooth.class);
                startActivity(intent1);
                break;
            case 2:
                Intent intent2 = new Intent(getActivity(), AccSync.class);
                startActivity(intent2);
                break;
            case 3:
                Intent intent3 = new Intent(getActivity(), Brightness.class);
                startActivity(intent3);
                break;
            case 4:
                Intent intent4 = new Intent(getActivity(), SoundMode.class);
                startActivity(intent4);
                break;
            case 5:
                Intent intent5 = new Intent(getActivity(), PowerManagerDeviceOff.class);
                startActivity(intent5);
                break;
            case 6:
                Intent intent6 = new Intent(getActivity(), PowerManagerReboot.class);
                startActivity(intent6);
                break;

        }
    }

}
