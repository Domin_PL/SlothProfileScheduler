package dominik.kedziak.sloth_profilescheduler.Fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;
import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import dominik.kedziak.sloth_profilescheduler.Adapters.rvRoomAdapter;
import dominik.kedziak.sloth_profilescheduler.R;
import dominik.kedziak.sloth_profilescheduler.Room.AlarmDatabase;
import dominik.kedziak.sloth_profilescheduler.Room.AlarmViewModel;
import dominik.kedziak.sloth_profilescheduler.Room.Alarms;
import dominik.kedziak.sloth_profilescheduler.Room.AlarmsDao;

public class UpcomingEvents extends Fragment
{
    private static final String TAG = UpcomingEvents.class.getSimpleName();
    private rvRoomAdapter roomAdapter;
    private RecyclerView mRecyclerView;
    private View emptyView;
    private Context context;

    public UpcomingEvents() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        this.context = null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.context = null;
    }

    @SuppressLint("RestrictedApi")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_upcoming_events, container, false);
        mRecyclerView = view.findViewById(R.id.recyclerview_main);
        emptyView = view.findViewById(R.id.empty_view);
        LinearLayoutManager linearLayoutManager =
                new LinearLayoutManager(context, RecyclerView.VERTICAL, false);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        roomAdapter = new rvRoomAdapter(context);
        mRecyclerView.setAdapter(roomAdapter);
        detectLayout();
        AlarmViewModel alarmViewModel = ViewModelProviders.of(this).get(AlarmViewModel.class);
        alarmViewModel.getallAlarms().observe(this, new Observer<List<Alarms>>() {
            @Override
            public void onChanged(List<Alarms> alarms) {
                roomAdapter.setAlarms(alarms);
                detectLayout();
            }
        });

        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView,
                                  @NonNull RecyclerView.ViewHolder viewHolder,
                                  @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                removeItem((long) viewHolder.itemView.getTag());

            }


            @Override
            public void onChildDrawOver(@NonNull Canvas c, @NonNull RecyclerView recyclerView,
                                        RecyclerView.ViewHolder viewHolder,
                                        float dX, float dY, int actionState, boolean isCurrentlyActive) {
                View itemView = viewHolder.itemView;
                if (viewHolder.getAdapterPosition() == -1) {
                    // not interested in those
                    return;
                }
                Paint paint = new Paint();
                Bitmap bitmap;
                if (dX < 0) {
                    paint.setColor(Color.RED);
                    bitmap = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.delete);
                    float height = (itemView.getHeight() / 2) - (bitmap.getHeight() / 2);
                    float bitmapWidth = bitmap.getWidth();

                    c.drawRect((float) itemView.getRight() + dX, (float) itemView.getTop(),
                            (float) itemView.getRight(), (float) itemView.getBottom(), paint);
                    c.drawBitmap(bitmap, ((float) itemView.getRight() - bitmapWidth) - 96f,
                            (float) itemView.getTop() + height, null);


                }
                super.onChildDrawOver(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }
        }).attachToRecyclerView(mRecyclerView);

        return view;
    }




    private void removeItem(final long id) {
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                AlarmDatabase db = AlarmDatabase.getInstance(getContext());
                AlarmsDao dao = db.alarmsDao();
                dao.deleteAlarm(id);
            }
        });}

    private void detectLayout() {
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                AlarmDatabase db = AlarmDatabase.getInstance(getContext());
                AlarmsDao dao = db.alarmsDao();
                final int count = dao.returnCount();
                Objects.requireNonNull(getActivity()).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (count > 0) {
                            emptyView.setVisibility(View.GONE);
                            mRecyclerView.setVisibility(View.VISIBLE);
                        } else {
                            mRecyclerView.setVisibility(View.GONE);
                            emptyView.setVisibility(View.VISIBLE);
                        }
                    }
                });
                Log.v("Count db", String.valueOf(count));

            }
        });
    }

}
