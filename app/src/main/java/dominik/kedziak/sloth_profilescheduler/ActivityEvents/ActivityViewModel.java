package dominik.kedziak.sloth_profilescheduler.ActivityEvents;

import android.app.Application;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import dominik.kedziak.sloth_profilescheduler.ActivityEvents.*;
import java.util.Calendar;

import androidx.lifecycle.AndroidViewModel;
import dominik.kedziak.sloth_profilescheduler.Room.Alarms;
import dominik.kedziak.sloth_profilescheduler.Room.Repository;

import static dominik.kedziak.sloth_profilescheduler.ActivityEvents.Enums.AlarmEnum.FONTSIZE;
import static dominik.kedziak.sloth_profilescheduler.ActivityEvents.Enums.AlarmEnum.IS24hCLOCK;

public class ActivityViewModel extends AndroidViewModel {

    private Repository mRepository;

    private Calendar calendar;
    private int year, month, day, hour, minute, spinnerOnOffMode, seekbarProgress;
    private boolean isPreviewChecked, is24H;
    private String time, date, title;

    private View mView;
    private Enums mEnum;

    void setOnViewChanged(View view) {
        this.mView = view;
    }

    public ActivityViewModel(Application application) {
        super(application);
        mRepository = new Repository(application);
        calendar = Calendar.getInstance();
        mEnum = new Enums();
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getApplication().getApplicationContext());
        setIs24H(sp.getBoolean(mEnum.getSPKey(IS24hCLOCK), false));
    }

    long saveAlarm(String title, String description, boolean isCustomSound) {
        Alarms alarms;
        alarms = (isCustomSound) ? new Alarms(title, date, time, year, month, day,
                hour, minute, description, 0, true, seekbarProgress)
                : new Alarms(title, date, time, year, month, day,
                hour, minute, description, 0, true);
        return mRepository.saveAlarm(alarms);
    }

    /**
     * This method is gets current date and time, assigns variables in ViewModel and
     * updates textviews within its values
     */
    void getCurrentDate() {
        //Calendar is initialized in ViewModel's constructor
        setYear(calendar.get(Calendar.YEAR));
        setMonth(calendar.get(Calendar.MONTH));
        setDay(calendar.get(Calendar.DAY_OF_MONTH));
        setHour(calendar.get(Calendar.HOUR_OF_DAY));
        setMinute(calendar.get(Calendar.MINUTE));

        updateTime();
        updateDate();
    }

    /**
     * Update textviews with values from ViewModel
     */
    void updateTime() {
        int hour = getHour();
        int mHour = hour;
        int minute = getMinute();
        boolean is24H = is24H();

        if (hour > 24 || hour < 0 || minute > 60 || minute < 0) return;
        //midnight is 12 AM ||  noon  is 12PM
        String time;
        if (!is24H && hour > 12 && hour != 24) {
            hour -= 12;
        }

        if (hour == 0 && !is24H) hour = 12;

        if (minute < 10) {
            time = hour + ":" + "0" + minute;
        } else {
            time = hour + ":" + minute;
        }

        // If 12h time format add a proper information
        if (!is24H) {
            time = (mHour <= 11) ? time + " AM" : time + " PM";
        }

        setTime(time);
        mView.onTimeChanged(time);
    }

    /**
     * Likely updateTime(), but with date
     */
    void updateDate() {
        int day = getDay();
        int month = getMonth();
        int year = getYear();
        if (day < 1 || day > 31 || month < 0 || month > 11 || year < 0) return;
        String mDay;
        mDay = String.valueOf(day);
        String date;
        if (day < 10) {
            mDay = "0" + day;
        }
        month += 1;

        date = (month < 10) ? mDay + "/0" + month + "/" + year
                : mDay + "/" + month + "/" + year;

        setDate(date);
        mView.onDateChanged(date);
    }


    float getFontSize() {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getApplication().getApplicationContext());
        return sp.getInt(mEnum.getSPKey(FONTSIZE), 15);
    }

    public Calendar getCalendar() {
        return calendar;
    }

    public void setCalendar(Calendar calendar) {
        this.calendar = calendar;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    public void setSpinnerOnOffMode(int spinnerOnOffMode) {
        this.spinnerOnOffMode = spinnerOnOffMode;
    }

    public int getSeekbarProgress() {
        return seekbarProgress;
    }

    public void setSeekbarProgress(int seekbarProgress) {
        this.seekbarProgress = seekbarProgress;
    }

    public boolean isPreviewChecked() {
        return isPreviewChecked;
    }

    public void setPreviewChecked(boolean previewChecked) {
        isPreviewChecked = previewChecked;
    }

    public boolean is24H() {
        return is24H;
    }

    public void setIs24H(boolean is24H) {
        this.is24H = is24H;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }



}
