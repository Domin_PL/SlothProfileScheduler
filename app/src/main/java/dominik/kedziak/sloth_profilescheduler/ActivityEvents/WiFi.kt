package dominik.kedziak.sloth_profilescheduler.ActivityEvents

import android.os.Bundle
import dominik.kedziak.sloth_profilescheduler.R

class WiFi : BaseClass() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setSpinnerSource(resources.getStringArray(R.array.spinnerWiFI))
    }


}