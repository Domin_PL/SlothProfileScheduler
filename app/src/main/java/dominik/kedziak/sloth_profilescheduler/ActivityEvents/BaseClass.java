package dominik.kedziak.sloth_profilescheduler.ActivityEvents;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.util.Calendar;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.lifecycle.ViewModelProviders;
import dominik.kedziak.sloth_profilescheduler.R;
import dominik.kedziak.sloth_profilescheduler.Tools.Permissions;
import dominik.kedziak.sloth_profilescheduler.Tools.SharedMethods;

import static dominik.kedziak.sloth_profilescheduler.ActivityEvents.Enums.AlarmEnum.FONTSIZE;
import static dominik.kedziak.sloth_profilescheduler.ActivityEvents.Enums.AlarmEnum.IS24hCLOCK;

public abstract class BaseClass extends AppCompatActivity implements
        dominik.kedziak.sloth_profilescheduler.ActivityEvents.View {

    protected abstract class BaseActivity extends BaseClass {}

    ActivityViewModel mViewModel;
    private TextView dateTextView;
    private TextView timeTextView;
    private TextView alarmTextView;
    private ConstraintLayout repeatLineConstraint;
    private Spinner AlarmSpinner;
    private Switch repeatSwitch;
    private View mSoundSettings;
    private ToggleButton monday, tuesday, wednesday, thursday, friday, saturday, sunday;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarm_class);
        mViewModel = ViewModelProviders.of(this).get(ActivityViewModel.class);
        initializeViews();
        setTitle(getResources().getString(R.string.newEvent));
        mViewModel.getCurrentDate();

    }

    //initialize views, assign variables to the views
    private void initializeViews(){
        mViewModel.setOnViewChanged(this);
        final float fontSize = mViewModel.getFontSize();

        TextView timeConstant = findViewById(R.id.time);
        TextView dateConstant = findViewById(R.id.date);
        TextView repeatConstant = findViewById(R.id.repeat);
        dateTextView = findViewById(R.id.dateSetter);
        timeTextView = findViewById(R.id.timeSetter);
        alarmTextView = findViewById(R.id.title);

        //setting proper text sizes
        timeConstant.setTextSize(fontSize);
        dateConstant.setTextSize(fontSize);
        repeatConstant.setTextSize(fontSize);
        dateTextView.setTextSize(fontSize);
        timeTextView.setTextSize(fontSize);
        //title is scalable fontSize + 10 sp
        alarmTextView.setTextSize(fontSize + 10);

        repeatLineConstraint = findViewById(R.id.tglBtnsLine);

        repeatSwitch = findViewById(R.id.repeatSwitch);

        repeatSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                new SharedMethods().onRepeatChanged(
                        repeatSwitch, repeatLineConstraint);
            }
        });

        monday = findViewById(R.id.monday);
        tuesday = findViewById(R.id.tuesday);
        wednesday = findViewById(R.id.wednesday);
        thursday = findViewById(R.id.thursday);
        friday = findViewById(R.id.friday);
        saturday = findViewById(R.id.saturday);
        sunday = findViewById(R.id.sunday);
        mSoundSettings = findViewById(R.id.seekbarLayout);

    }




    /*
     * this method is called after line containing time is pressed
     * when time is set assign chosen values to the variables and call updateTime() to
     * update results in the TextViews
     */
    public void setTime(View v){
        TimePickerDialog timePicker = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                mViewModel.setHour(hourOfDay);
                mViewModel.setMinute(minute);
                mViewModel.updateTime();
            }
        }, mViewModel.getHour(), mViewModel.getMinute(), mViewModel.is24H());
        timePicker.show();
    }




    /**
     * Likely setTime(), but sets date
     */

    public void setDate(View v){
        DatePickerDialog datePicker = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                mViewModel.setYear(year);
                mViewModel.setMonth(month);
                mViewModel.setDay(dayOfMonth);
                mViewModel.updateDate();
            }
        }, mViewModel.getYear(), mViewModel.getMonth(), mViewModel.getDay());
        datePicker.show();
    }



    public void lineRepeat(View view){
        new SharedMethods().setRepeatChanged(repeatSwitch, repeatLineConstraint);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        MenuItem menuItem = menu.findItem(R.id.dismiss);
        menuItem.setVisible(false);
        return true;
    }

    @Override
    public void invalidateOptionsMenu() {
        super.invalidateOptionsMenu();
    }

    /**
     * This method is called when saveAlarm button (in menu) is clicked
     * This sets up calendar with variables from ViewModel and assigns new calendar back to the ViewModel
     * After it's done it calls saveAlarm()
     */

    protected void Alarm() {

        Calendar calendar = mViewModel.getCalendar();
        calendar.set(
                mViewModel.getYear(),
                mViewModel.getMonth(),
                mViewModel.getDay(),
                mViewModel.getHour(),
                mViewModel.getMinute(),
                0);
        mViewModel.setCalendar(calendar);
        saveAlarm();
    }

    /**
     * This method is called from Alarm()
     * This is responsible for preparing rest of variables to schedule an alarm.
     * When everything is ready, it calls ViewModel --> Repository --> Dao, which saves data and returns
     * row id, when row id is returned IT IS AN ALARM ID. This ID is assigned to the intent,
     * and later retrieved from class extending IntentService to read a row from database
     */

    private void saveAlarm() {

        final String title, description;
        title = mViewModel.getTitle();

        description = new SharedMethods().releaseRepeat(getApplicationContext(),
                monday, tuesday, wednesday,
                thursday, friday, saturday,
                sunday);

        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                long id = mViewModel.saveAlarm(title, description,
                        mSoundSettings.getVisibility() == View.VISIBLE);
                Log.d("IDs", String.valueOf(id));

                Uri newUri = Uri.parse(String.valueOf(id));

                if (monday.isChecked() || tuesday.isChecked() || wednesday.isChecked()
                        || thursday.isChecked() || friday.isChecked()
                        || saturday.isChecked() || sunday.isChecked()) {
                    new AlarmScheduler(getApplicationContext(), title, mViewModel.getCalendar().getTimeInMillis(),
                            newUri).setAlarmRepeat();
                } else {
                    new AlarmScheduler(getApplicationContext(), title, mViewModel.getCalendar().getTimeInMillis(),
                            newUri).setAlarm();
                }
            }
        });
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.tick:
                Alarm();
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * This method hides spinner when it's not needed, e.g when activity offers
     * only one choice e.g turn device off
     */
    public void hideSpinner(){
        View spinnerView = findViewById(R.id.spinner_layout);
        spinnerView.setVisibility(View.GONE);
    }

    /**
     * This method assigns alarm name to ViewModel variable and updates text on the screen
     * @param title alarm title
     */
    public void setAlarmTitle(String title){
        mViewModel.setTitle(title);
        updateAlarmText();
    }

    /**
     * This method updates text in the TextView
     */
    void updateAlarmText(){
        alarmTextView.setText(mViewModel.getTitle());
    }

    /**
     * This method initializes spinner, sets array as adapter and sets up
     * onItemSelectedListener, which updates ViewModel variables and text on the screen
     * @param array array source to adapter
     */
    public void setSpinnerSource(final String[] array){
        AlarmSpinner = findViewById(R.id.spinnerOnOff);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this,
                android.R.layout.simple_spinner_item,
                array);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        AlarmSpinner.setAdapter(adapter);
        AlarmSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                mViewModel.setSpinnerOnOffMode(position);
                setAlarmTitle(array[position]);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    public void setCustomSpinnerListener(final String[] array){
        AlarmSpinner = findViewById(R.id.spinnerOnOff);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this,
                android.R.layout.simple_spinner_item,
                array);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        AlarmSpinner.setAdapter(adapter);
        AlarmSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position){
                    case 0:
                        mViewModel.setSpinnerOnOffMode(position);
                        setAlarmTitle(array[position]);
                        hideSeekbar();
                        break;
                    case 1:
                        mViewModel.setSpinnerOnOffMode(position);
                        setAlarmTitle(array[position]);
                        hideSeekbar();
                        break;
                    case 2:
                        mViewModel.setSpinnerOnOffMode(position);
                        setAlarmTitle(array[position]);
                        showSeekbar();
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void hideSeekbar(){
        if (mSoundSettings.getVisibility() == View.VISIBLE) {
            mSoundSettings.setVisibility(View.GONE);
        }
    }

    private void showSeekbar(){
        if (mSoundSettings.getVisibility() == View.GONE) {
            mSoundSettings.setVisibility(View.VISIBLE);
            SeekBar mSoundSeekbar = findViewById(R.id.seekBar);
            Switch mPreviewSwitch = findViewById(R.id.previewSwitch);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (!Settings.System.canWrite(getApplicationContext())){
                    Toast.makeText(
                            getApplicationContext(), getString(R.string.noPrivileges),
                            Toast.LENGTH_SHORT).show();
                    return;
                }
            }

            Settings.System.putInt(getContentResolver(),
                    Settings.System.SCREEN_BRIGHTNESS_MODE, Settings.System.SCREEN_BRIGHTNESS_MODE_MANUAL);
            try {
                mViewModel.setSeekbarProgress(Settings.System.getInt(getContentResolver(),
                        Settings.System.SCREEN_BRIGHTNESS));
            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
            }
            mSoundSeekbar.setProgress(mViewModel.getSeekbarProgress());
            mSoundSeekbar.setMax(255);
            mPreviewSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    mViewModel.setPreviewChecked(isChecked);
                }
            });
            mSoundSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    mViewModel.setSeekbarProgress(progress);
                    if (mViewModel.isPreviewChecked()){
                        Settings.System.putInt(getContentResolver(), Settings.System.SCREEN_BRIGHTNESS,
                                progress);
                    }
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {
                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                }
            });
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    void getSystemPermission(){
        new Permissions().modifySettings(getApplicationContext());
    }

    void checkRootPermission(){
        new Permissions().isRootAvaible();
    }


    @Override
    public void onTimeChanged(String time) {
        timeTextView.setText(time);
    }

    @Override
    public void onDateChanged(String date) {
        dateTextView.setText(date);
    }

}
