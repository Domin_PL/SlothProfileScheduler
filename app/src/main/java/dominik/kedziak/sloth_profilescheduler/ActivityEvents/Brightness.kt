package dominik.kedziak.sloth_profilescheduler.ActivityEvents

import android.os.Build
import android.os.Bundle
import dominik.kedziak.sloth_profilescheduler.R

class Brightness : BaseClass() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setCustomSpinnerListener(applicationContext.resources
                .getStringArray(R.array.brightness))
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getSystemPermission()
        }
    }
}