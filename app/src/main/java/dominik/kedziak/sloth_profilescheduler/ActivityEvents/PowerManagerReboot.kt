package dominik.kedziak.sloth_profilescheduler.ActivityEvents

import android.os.Bundle
import dominik.kedziak.sloth_profilescheduler.R

class PowerManagerReboot : BaseClass(){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setAlarmTitle(getString(R.string.reboot))
        hideSpinner()
        checkRootPermission()
    }
}