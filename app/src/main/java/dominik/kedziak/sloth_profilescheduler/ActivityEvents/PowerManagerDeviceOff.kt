package dominik.kedziak.sloth_profilescheduler.ActivityEvents

import android.os.Bundle
import dominik.kedziak.sloth_profilescheduler.R

class PowerManagerDeviceOff : BaseClass(){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setAlarmTitle(getString(R.string.turnOff))
        hideSpinner()
        checkRootPermission()
    }
}