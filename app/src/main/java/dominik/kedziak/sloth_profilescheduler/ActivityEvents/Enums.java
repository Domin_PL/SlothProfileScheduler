package dominik.kedziak.sloth_profilescheduler.ActivityEvents;

import java.io.Serializable;
import java.util.HashMap;

import static dominik.kedziak.sloth_profilescheduler.ActivityEvents.Enums.AlarmEnum.*;

class Enums {

    private HashMap<AlarmEnum, Serializable> map;

    enum AlarmEnum {
        INTERVAL_TIME_IN_MILLIS,
        IS24hCLOCK,
        IS_NOTIFICATION,
        FONTSIZE;
    }

    Enums() {
        map = new HashMap<>();
        map.put(INTERVAL_TIME_IN_MILLIS, 86400000);
        map.put(IS24hCLOCK, "is24hClock");
        map.put(IS_NOTIFICATION, "isNotification");
        map.put(FONTSIZE, "fontSize");
    }

    String getSPKey(AlarmEnum Enum){
        return (Enum.equals(AlarmEnum.INTERVAL_TIME_IN_MILLIS)) ? null : (String) map.get(Enum);
    }


}




