package dominik.kedziak.sloth_profilescheduler.ActivityEvents;

public interface View {

    void onTimeChanged(String time);
    void onDateChanged(String date);
}
