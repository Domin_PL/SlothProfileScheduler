package dominik.kedziak.sloth_profilescheduler.ActivityEvents;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

import dominik.kedziak.sloth_profilescheduler.AccountSyncScheduler.AccSyncScheduler;
import dominik.kedziak.sloth_profilescheduler.BluetoothScheduler.BluetoothScheduler;
import dominik.kedziak.sloth_profilescheduler.BrightnessScheduler.BrightnessScheduler;
import dominik.kedziak.sloth_profilescheduler.PowerManagerScheduler.PowerManagerScheduler;
import dominik.kedziak.sloth_profilescheduler.R;
import dominik.kedziak.sloth_profilescheduler.SoundModeScheduler.SoundScheduler;
import dominik.kedziak.sloth_profilescheduler.WiFiScheduler.WiFiScheduler;

class AlarmScheduler {

    private Context mContext;
    private String mTitle;
    private long mTimeInMilis;
    private Uri mUri;

    AlarmScheduler(Context mContext, String mTitle, long mTimeInMilis, Uri mUri) {
        this.mContext = mContext;
        this.mTitle = mTitle;
        this.mTimeInMilis = mTimeInMilis;
        this.mUri = mUri;
    }



    void setAlarm(){
        Log.v("Title", mTitle);
        if (mTitle.equals(mContext.getString(R.string.wifiOff))) {
            new WiFiScheduler().setAlarmOff(mContext, mTimeInMilis, mUri);
        } else if (mTitle.equals(mContext.getString(R.string.wifiOn))) {
            new WiFiScheduler().setAlarmOn(mContext, mTimeInMilis, mUri);
        } else if (mTitle.equals(mContext.getString(R.string.loudMode))) {
            new SoundScheduler().setAlarmLoud(mContext, mTimeInMilis, mUri);
        } else if (mTitle.equals(mContext.getString(R.string.muteMode))) {
            new SoundScheduler().setAlarmMute(mContext, mTimeInMilis, mUri);
        } else if (mTitle.equals(mContext.getString(R.string.disturbMode))) {
            new SoundScheduler().setAlarmDisturb(mContext, mTimeInMilis, mUri);
        } else if (mTitle.equals(mContext.getString(R.string.reboot))) {
            new PowerManagerScheduler().setAlarmReboot(mContext, mTimeInMilis, mUri);
        } else if (mTitle.equals(mContext.getString(R.string.turnOff))) {
            new PowerManagerScheduler().setAlarmOff(mContext, mTimeInMilis, mUri);
        } else if (mTitle.equals(mContext.getString(R.string.brightScreen))) {
            new BrightnessScheduler().setAlarmBright(mContext, mTimeInMilis, mUri);
        } else if (mTitle.equals(mContext.getString(R.string.darkScreen))) {
            new BrightnessScheduler().setAlarmDark(mContext, mTimeInMilis, mUri);
        } else if (mTitle.equals(mContext.getString(R.string.customScreen))) {
            new BrightnessScheduler().setAlarmCustom(mContext, mTimeInMilis, mUri);
        } else if (mTitle.equals(mContext.getString(R.string.btOn))) {
            new BluetoothScheduler().setAlarmOn(mContext, mTimeInMilis, mUri);
        } else if (mTitle.equals(mContext.getString(R.string.btOff))) {
            new BluetoothScheduler().setAlarmOff(mContext, mTimeInMilis, mUri);
        } else if (mTitle.equals(mContext.getString(R.string.accSyncOff))) {
            new AccSyncScheduler().setAlarmOff(mContext, mTimeInMilis, mUri);
        } else if (mTitle.equals(mContext.getString(R.string.accSyncOn))){
            new AccSyncScheduler().setAlarmOn(mContext, mTimeInMilis, mUri);
        } else if (mTitle.contains(mContext.getString(R.string.profil))){
            new SoundScheduler().setAlarmCustom(mContext, mTimeInMilis, mUri);
        } else {
            Log.e("Failed scheduling alarm", "No alarm found");
        }
    }
    
    void setAlarmRepeat(){
        final long mDailyConst = 86400000L;
        if (mTitle.equals(mContext.getString(R.string.wifiOff))) { //WiFi off service
            new WiFiScheduler().setAlarmOffDaily(mContext, mTimeInMilis, mUri, mDailyConst);
        } else if (mTitle.equals(mContext.getString(R.string.wifiOn))) { // WiFi on
            new WiFiScheduler().setAlarmOnDaily(mContext, mTimeInMilis, mUri, mDailyConst);
        } else if (mTitle.equals(mContext.getString(R.string.loudMode))) { // Loud mode
            new SoundScheduler().setAlarmLoudDaily(mContext, mTimeInMilis, mUri, mDailyConst);
        } else if (mTitle.equals(mContext.getString(R.string.muteMode))) { // Mute mode
            new SoundScheduler().setAlarmMuteDaily(mContext, mTimeInMilis, mUri, mDailyConst);
        } else if (mTitle.equals(mContext.getString(R.string.disturbMode))) { // Do not disturb
            new SoundScheduler().setAlarmDisturbDaily(mContext, mTimeInMilis, mUri, mDailyConst);
        } else if (mTitle.equals(mContext.getString(R.string.reboot))) { // reboot
            new PowerManagerScheduler().setAlarmRebootDaily(mContext, mTimeInMilis, mUri, mDailyConst);
        } else if (mTitle.equals(mContext.getString(R.string.turnOff))) { // turn off
            new PowerManagerScheduler().setAlarmOffDaily(mContext, mTimeInMilis, mUri, mDailyConst);
        } else if (mTitle.equals(mContext.getString(R.string.brightScreen))) { // bright screen
            new BrightnessScheduler().setDailyAlarmBright(mContext,mTimeInMilis, mUri, mDailyConst);
        } else if (mTitle.equals(mContext.getString(R.string.darkScreen))) {   // dark screen
            new BrightnessScheduler().setDailyAlarmDark(mContext, mTimeInMilis, mUri, mDailyConst);
        } else if (mTitle.equals(mContext.getString(R.string.customScreen))) { // custom screen
            new BrightnessScheduler().setDailyAlarmCustom(mContext, mTimeInMilis, mUri, mDailyConst);
        } else if (mTitle.equals(mContext.getString(R.string.btOn))) {  // bt on
            new BluetoothScheduler().setDailyAlarmOn(mContext, mTimeInMilis, mUri, mDailyConst);
        } else if (mTitle.equals(mContext.getString(R.string.btOff))) { // bt off
            new BluetoothScheduler().setDailyAlarmOff(mContext, mTimeInMilis, mUri, mDailyConst);
        }else if (mTitle.equals(mContext.getString(R.string.accSyncOff))) { // acc off
            new AccSyncScheduler().setDailyAlarmOff(mContext, mTimeInMilis, mUri, mDailyConst);
        } else if (mTitle.equals(mContext.getString(R.string.accSyncOn))){  // acc on
            new AccSyncScheduler().setDailyAlarmOn(mContext, mTimeInMilis, mUri, mDailyConst);
        } else if(mTitle.contains(mContext.getString(R.string.profil))) { // sound profile
            new SoundScheduler().setAlarmCustomDaily(mContext,mTimeInMilis, mUri, mDailyConst);
        } else {
            Log.e("Failed scheduling alarm", "No alarm found");
        }
    }
}
