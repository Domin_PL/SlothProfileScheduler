package dominik.kedziak.sloth_profilescheduler.ActivityEvents

import android.os.Bundle
import dominik.kedziak.sloth_profilescheduler.R

 class AccSync : BaseClass() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val array = resources.getStringArray(R.array.accsync)
        setSpinnerSource(array)
    }

}