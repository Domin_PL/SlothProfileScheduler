package dominik.kedziak.sloth_profilescheduler;

import android.app.NotificationManager;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.provider.Settings;
import android.view.MenuItem;

import com.google.android.material.navigation.NavigationView;
import com.google.android.material.tabs.TabLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.viewpager.widget.ViewPager;
import dominik.kedziak.sloth_profilescheduler.Adapters.ViewPagerAdapter;
import dominik.kedziak.sloth_profilescheduler.Fragments.DataActions;
import dominik.kedziak.sloth_profilescheduler.Fragments.UpcomingEvents;
import dominik.kedziak.sloth_profilescheduler.Preferences.BrightnessSettings;
import dominik.kedziak.sloth_profilescheduler.Preferences.Preferences;
import dominik.kedziak.sloth_profilescheduler.Preferences.SoundSettings;
import dominik.kedziak.sloth_profilescheduler.Tools.RootTools;

import static dominik.kedziak.sloth_profilescheduler.Tools.RootTools.isRootGranted;

//        Copyright 2018 Dominik Kędziak
//
//        Licensed under the Apache License, Version 2.0 (the "License");
//        you may not use this file except in compliance with the License.
//        You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//        Unless required by applicable law or agreed to in writing, software
//        distributed under the License is distributed on an "AS IS" BASIS,
//        WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//        See the License for the specific language governing permissions and
//        limitations under the License.

/*
 * This class is the main class of application. It is responsible for showing viewpager with 2 fragments,
 * inflating DrawerLayout and responding to its items.
 */

/**
 * When you will be reviewing Services classes, take a note that only SoundActivity and SoundServices
 * will be commented. Other services classes are very similar, so there is no point in recopying same comments
 */

public class MainActivity extends AppCompatActivity {

    private DrawerLayout drawerLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_main);

        //inflate viewpager
        ViewPager viewPager = findViewById(R.id.pager);
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new DataActions(), getString(R.string.dataActions));
        adapter.addFragment(new UpcomingEvents(), getString(R.string.upcomingEvents));
        viewPager.setOffscreenPageLimit(2);
        viewPager.setAdapter(adapter);
        drawerLayout = findViewById(R.id.drawer_layout);

        //inflate tablayout
        TabLayout tabLayout = findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        //inflate navigationView and respond to its items
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        drawerLayout.closeDrawers();
                        item.setChecked(false);
                        int itemId = item.getItemId();
                        switch (itemId) {
                            //brightness preferences
                            case R.id.brightness:
                                Intent intent = new Intent(MainActivity.this,
                                        BrightnessSettings.class);
                                startActivity(intent);
                                break;
                            //rate app
                            case R.id.rate:
                                rateApp();
                                break;
                            //ask for permissions
                            case R.id.permissions:
                                askForPermissions();
                                break;
                            //app preferences
                            case R.id.preferences:
                                Intent prefIntent = new Intent(MainActivity.this, Preferences.class);
                                startActivity(prefIntent);
                                break;
                            //sound preferences
                            case R.id.soundSettings:
                                Intent soundIntent = new Intent(MainActivity.this, SoundSettings.class);
                                startActivity(soundIntent);
                                break;
                        }
                        return true;
                    }
                }
        );
    }

    //open Play Store with application page
    private void rateApp() {
        Uri uri = Uri.parse("market://details?id=" + getApplicationContext().getPackageName());
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
        // To count with Play market backstack, After pressing back button,
        // to taken back to our application, we need to add following flags to intent.
        goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        try {
            startActivity(goToMarket);
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id=" + getApplicationContext().getPackageName())));
        }
    }

    //ask for all required permissions in the app. This is handled by navigationView item
    private void askForPermissions() {
        //battery optimizations
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Intent i = new Intent();
            String packageName = getPackageName();
            PowerManager pm = (PowerManager) getSystemService(POWER_SERVICE);
            if (!pm.isIgnoringBatteryOptimizations(packageName)) {
                i.setAction(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
                i.setData(Uri.parse("package:" + packageName));
                startActivity(i);
            }
        }

        //write secure settings
        if (Build.VERSION.SDK_INT > 23) {
            boolean canWrite = Settings.System.canWrite(getApplicationContext());
            if (!canWrite) {
                Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS);
                intent.setData(Uri.parse("package:" + getApplicationContext().getPackageName()));
                startActivity(intent);
            }
            accessSettings();
            //root permissions
            boolean isGranted = permissionGranted();
            if (!isGranted){
                RootTools.isRootGranted();

            }
        }
    }
    //check if root privileges are given
    private boolean permissionGranted(){
        return isRootGranted();
    }

    //access settings in order to get permission to notification service
    //  (needed to launch do not disturb spinnerMode)
    private void accessSettings(){
        NotificationManager notificationManager =
                (NotificationManager) getBaseContext().getSystemService(Context.NOTIFICATION_SERVICE);

        assert notificationManager != null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                && !notificationManager.isNotificationPolicyAccessGranted()) {

            Intent i = new Intent(
                    android.provider.Settings
                            .ACTION_NOTIFICATION_POLICY_ACCESS_SETTINGS);

            startActivity(i);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        finish();
    }
}