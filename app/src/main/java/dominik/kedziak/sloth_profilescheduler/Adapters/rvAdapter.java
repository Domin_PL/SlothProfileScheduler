package dominik.kedziak.sloth_profilescheduler.Adapters;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import dominik.kedziak.sloth_profilescheduler.R;

public class rvAdapter extends RecyclerView.Adapter<rvAdapter.ViewHolder> {

    private List<String> mActions;
    private List<String> mDescription;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;

    // data is passed into the constructor
    public rvAdapter(Context context, List<String> data, List<String> description) {
        mInflater = LayoutInflater.from(context);
        this.mActions = data;
        this.mDescription = description;
    }
    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_recyclerview_list, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String animal = mActions.get(position);
        holder.actionsText.setText(animal);
        String description = mDescription.get(position);
        holder.descriptionText.setText(description);
    }

    // total number of rows
    @Override
    public int getItemCount() {
        return mActions.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView actionsText;
        TextView descriptionText;

        ViewHolder(View itemView) {
            super(itemView);
            actionsText = itemView.findViewById(R.id.action);
            descriptionText = itemView.findViewById(R.id.description);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }


    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}
