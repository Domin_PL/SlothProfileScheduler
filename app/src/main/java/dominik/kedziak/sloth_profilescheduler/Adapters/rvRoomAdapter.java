package dominik.kedziak.sloth_profilescheduler.Adapters;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import dominik.kedziak.sloth_profilescheduler.R;
import dominik.kedziak.sloth_profilescheduler.Room.AlarmDatabase;
import dominik.kedziak.sloth_profilescheduler.Room.Alarms;
import dominik.kedziak.sloth_profilescheduler.Room.AlarmsDao;
import dominik.kedziak.sloth_profilescheduler.Tools.AdapterHelper;
import dominik.kedziak.sloth_profilescheduler.Tools.adapterCalendarTools;
import dominik.kedziak.sloth_profilescheduler.Tools.validationUpdater;

public class rvRoomAdapter extends RecyclerView.Adapter<rvRoomAdapter.AlarmViewHolder> {


    class AlarmViewHolder extends RecyclerView.ViewHolder {

        /*
         * Initialize all variables. Take a note on its naming as following:
         * variableTextView -  for TextViews
         * variableSwitch - for Switches etc
         * only ToggleButtons are called as day of week
         */
        private TextView titleTextView;
        private TextView timeDateTextView;
        //Switch to hold alarm validation (on/off)
        private Switch isAlarmActiveSwitch;
        //arrows to expand/hide expandable view
        private ImageButton arrowDownImgBtn;
        private ImageButton arrowUpImgBtn;
        //Layout that is shown when buttons above are clicked
        private View extenstionLayout;
        //Checkbox detecting if user wants alarm to be repeated or not
        private CheckBox isRepeatingCheckBox;
        //time and date TextViews, click on them to open date/timepicker
        private TextView timeSetterTextView;
        private TextView dateSetterTextView;
        //ToggleButtons
        private ToggleButton monday;
        private ToggleButton tuesday;
        private ToggleButton wednesday;
        private ToggleButton thursday;
        private ToggleButton friday;
        private ToggleButton saturday;
        private ToggleButton sunday;
        //delete button to delete item (it's held as whole layout)
        private ConstraintLayout deleteConstraint;
        //delete button to delete item
        private Button doneButton;
        //constraintlayout to extense rest layout (item layout)
        private ConstraintLayout itemConstraint;
        //constraintLayout with toggleButtons
        private ConstraintLayout toggleButtonsConstraint;


        AlarmViewHolder(@NonNull View view) {
            super(view);
            //initialize variables
            titleTextView = view.findViewById(R.id.recycle_title);
            timeDateTextView = view.findViewById(R.id.recycle_date_time);
            isAlarmActiveSwitch = view.findViewById(R.id.isActive);
            arrowDownImgBtn = view.findViewById(R.id.arrowDown);
            arrowUpImgBtn = view.findViewById(R.id.arrowUp);
            extenstionLayout = view.findViewById(R.id.alarm_helper);
            isRepeatingCheckBox = view.findViewById(R.id.repeat);
            timeSetterTextView = view.findViewById(R.id.setTime);
            dateSetterTextView = view.findViewById(R.id.setDate);
            monday = view.findViewById(R.id.monday);
            tuesday = view.findViewById(R.id.tuesday);
            wednesday = view.findViewById(R.id.wednesday);
            thursday = view.findViewById(R.id.thursday);
            friday = view.findViewById(R.id.friday);
            saturday = view.findViewById(R.id.saturday);
            sunday = view.findViewById(R.id.sunday);
            deleteConstraint = view.findViewById(R.id.deleteLayout);
            doneButton = view.findViewById(R.id.doneButton);
            itemConstraint = view.findViewById(R.id.constrainto);
            toggleButtonsConstraint = view.findViewById(R.id.tglBtnsConstraint);

        }
    }

    private final LayoutInflater mInflater;
    private Context context;
    private List<Alarms> mAlarms;


    public rvRoomAdapter(Context context){
        mInflater = LayoutInflater.from(context);
        this.context = context;
    }



    @NonNull
    @Override
    public AlarmViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_recyclerview_alarms, parent, false);
        return new AlarmViewHolder(view);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(@NonNull final AlarmViewHolder holder, int position) {
        if (mAlarms != null){
            //get data for current adapter position
            final Alarms currentData = mAlarms.get(position);
            //I will initialize Calendar instance here that will be used later in whole
            // alarm process. This is the most important variable so that it must be
            // easilly to find
            // get Calendar instance
            final Calendar calendar = Calendar.getInstance();
            //let's assign proper fields to calendar
            calendar.set(currentData.getYear(), currentData.getMonth(), currentData.getDay(),
                    currentData.getHour(), currentData.getMinute(), 0);
            //let's get instance of adapterHelper.kt class
            final AdapterHelper adapterHelper = new AdapterHelper();

            //we'll use few times ToggleButtons, let's saveAlarm their instances as local variables
            final ToggleButton monday = holder.monday;
            final ToggleButton tuesday = holder.tuesday;
            final ToggleButton wednesday = holder.wednesday;
            final ToggleButton thursday = holder.thursday;
            final ToggleButton friday = holder.friday;
            final ToggleButton saturday = holder.saturday;
            final ToggleButton sunday = holder.sunday;
            //assign values to proper fields
            //set up alarm title
            holder.titleTextView.setText(currentData.getTitle());
            //set up tag id -  it's id from DATABASE, not adapter
            holder.itemView.setTag(currentData.get_id());
            //let's break through strings we got from database:
            //let's define if alarm is set up as daily or repeating or just once

            //instance of daily string
            final String dailyString = context.getString(R.string.repeat1Day);
            //get repeat string from database
            final String repeatString = currentData.getRepeat();
            //let's initialize alarm description - date, time and repetition
            String alarmDescriptionString = "";
            //let's get date and time from database to construct the string
            final String date = currentData.getDate();
            String time = currentData.getTime();
            //let's get string "at"
            String at = context.getString(R.string.at);
            /*
             *if returnRepeatString is empty, the description is to be look like:
             * date + at + time e.g 22/09/2018 at 8 PM
             * or: Repeat daily at time  // if alarm is repeat daily
             * Repeat on /repeat days/ + at time //if alarm is on specified day week
             * e.g Repeat on Mon, Tue, Fri at 4 PM
             */

            if (TextUtils.isEmpty(repeatString)){
                alarmDescriptionString = date + " " + at + " " + time;
                holder.timeDateTextView.setText(alarmDescriptionString);
            } else if (repeatString != null && repeatString.equals(dailyString)){
                alarmDescriptionString = dailyString + " " + at + " " + time;
                holder.timeDateTextView.setText(alarmDescriptionString);
            } else if (repeatString != null && !repeatString.equals(dailyString)){
                //because repeat here ends up with ", " we don't want those, we need to delete them
                String repString = adapterHelper.deleteChar(repeatString);
                alarmDescriptionString = repString + " " + at + " "
                        + time;
                holder.timeDateTextView.setText(alarmDescriptionString);
            }

            //check if alarm is valid and then set this value to the switch
            holder.isAlarmActiveSwitch.setChecked(currentData.isValid());

            //by now switch button doesn't disable/enable alarm, let's set up onSwitchChangedListener
            //and disable/enable alarm by updating proper db row with boolean
            new validationUpdater().switchChangedListener(holder.isAlarmActiveSwitch,
                    context, currentData.get_id());

            /*
             * Let's set up layout click listeners -  we can expand and hide layout in 2 ways
             * Click on Alarm title, its description -  or simply on arrow button
             * We do not set whole item to be listener.
             */

            adapterHelper.clickListeners(holder.titleTextView, holder.timeDateTextView,
                    holder.arrowDownImgBtn, holder.arrowUpImgBtn, holder.extenstionLayout, context);

            /*
             * Layout extension
             */

            //if returnRepeatString is empty, it means that it is single alarm, we need to then
            // uncheck repeat checkbox, otherwise set checkbox and show ToggleButtons and set correct one
           holder.extenstionLayout.setTag(holder.extenstionLayout.getVisibility());
           holder.extenstionLayout.getViewTreeObserver().addOnGlobalLayoutListener
                   (new ViewTreeObserver.OnGlobalLayoutListener() {
               @Override
               public void onGlobalLayout() {
                   int visibility = holder.extenstionLayout.getVisibility();
                   if ((int)holder.extenstionLayout.getTag() != visibility){
                       holder.extenstionLayout.setTag(visibility);

                       if (holder.extenstionLayout.getVisibility() == View.VISIBLE) {
                           if (repeatString != null) {
                               //returnRepeatString will be never empty, so we can't use TextUtils
                               //to detect if it is one time alarm
                               if (repeatString.equals("")) {
                                   holder.isRepeatingCheckBox.setChecked(false);
                               } else {
                                   holder.isRepeatingCheckBox.setChecked(true);
                               }
                           }
                           if (repeatString != null && repeatString.equals(dailyString)) {
                               holder.isRepeatingCheckBox.setChecked(true);
                               holder.toggleButtonsConstraint.setVisibility(View.VISIBLE);
                               monday.setChecked(true);
                               tuesday.setChecked(true);
                               wednesday.setChecked(true);
                               thursday.setChecked(true);
                               friday.setChecked(true);
                               saturday.setChecked(true);
                               sunday.setChecked(true);
                           }
                           if (repeatString != null && !repeatString.equals("")) {
                               holder.isRepeatingCheckBox.setChecked(true);
                               holder.toggleButtonsConstraint.setVisibility(View.VISIBLE);
                               adapterHelper.breakString(repeatString, context, monday, tuesday, wednesday, thursday, friday,
                                       saturday, sunday);
                           }

                           //in the end, set up onChecboxChanged listener - if unchecked hide toggle buttons
                           adapterHelper.setOnRepeatCheckboxChangedListener(holder.isRepeatingCheckBox, holder.toggleButtonsConstraint);

                           //set time and date setters
                           holder.timeSetterTextView.setText(currentData.getTime());
                           holder.dateSetterTextView.setText(currentData.getDate());


                           holder.timeSetterTextView.setOnClickListener(new View.OnClickListener() {
                               @Override
                               public void onClick(View v) {
                                   timePicker(context, holder.timeSetterTextView, calendar);
                               }
                           });

                           holder.dateSetterTextView.setOnClickListener(new View.OnClickListener() {
                               @Override
                               public void onClick(View v) {
                                    datePicker(context, holder.dateSetterTextView, calendar);
                               }
                           });

                           //let's define onDeleteClickListener to delete specified alarm
                           holder.deleteConstraint.setOnClickListener(new View.OnClickListener() {
                               @Override
                               public void onClick(View v) {
                                   AsyncTask.execute(new Runnable() {
                                       @Override
                                       public void run() {
                                           AlarmDatabase db = AlarmDatabase.getInstance(context);
                                           AlarmsDao dao = db.alarmsDao();
                                           dao.deleteAlarm((Long) holder.itemView.getTag());
                                       }
                                   });
                                   Toast.makeText(context, R.string.alarmDeleted, Toast.LENGTH_SHORT).show();
                               }
                           });

                           /*
                            * This is the most important onClick method. It is responsible for
                            * saving alarm, updating proper values, setting up proper intentService
                            * to fire.
                            */

                           holder.doneButton.setOnClickListener(new View.OnClickListener() {
                               @Override
                               public void onClick(View v) {

                                   //let's configure if alarm is repeating or just one fire only
                                   Boolean isRepeatBoolean = monday.isChecked() || tuesday.isChecked()
                                           || wednesday.isChecked() || thursday.isChecked() ||
                                           friday.isChecked() || saturday.isChecked() || sunday.isChecked();

                                   //let's combine description string
                                   String repeatString = new adapterCalendarTools().releaseRepeat(context, monday, tuesday,
                                           wednesday, thursday, friday, saturday, sunday);

                                   //let's figure out what kind of service it is
                                   String service = currentData.getTitle();

                                   //we won't pass URI anymore to schedule an alarm. We will pass database
                                   //id so that we will search a row where id equals alarm id

                                   new adapterCalendarTools().saveAlarm(context, (Long)holder.itemView.getTag(),
                                           service, calendar,isRepeatBoolean, repeatString );

                                   //hide extension layout
                                   holder.extenstionLayout.setVisibility(View.GONE);
//
                               }
                           });

                       }
                   }
               }
           });


        }
    }

    public void setAlarms(List<Alarms> alarms){
        mAlarms = alarms;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if (mAlarms != null){
            return mAlarms.size();
        } else {
            return 0;
        }
    }


    /*
     * Set up timePickerDialog that will show the timepicker in window to the user
     * Use SharedPreferences to read whether user wants to use 12h Clock or 24h format
     * Set up calendar after user sets willing time and assign its value to instance of Calendar
     */
    private void timePicker(Context context, final TextView timeText, final Calendar calendar){
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        String is24H = "is24hClock";
        final Boolean is24Clock = sp.getBoolean(is24H, false);
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);
        TimePickerDialog timePickerDialog = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                   calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                   calendar.set(Calendar.MINUTE, minute);
                   String mTime;
                   int hour = hourOfDay;
                if (!is24Clock && hourOfDay > 13 && hourOfDay != 24) {
                    hour -= 12;
                }
                if (minute < 10) {
                    mTime = String.valueOf(hour) + ":" + "0" + minute;
                } else {
                    mTime = String.valueOf(hour) + ":" + minute;
                }


                if (!is24Clock && hourOfDay < 12) {
                    mTime += " AM";
                } else if (!is24Clock) {
                    mTime += " PM";
                }
                timeText.setText(mTime);
            }
        }, hour, minute, is24Clock);

        timePickerDialog.show();

    }

    /**
     *
     * @param context app context
     * @param dateText textview which on click opens this dialog and outcome of this dialog is
     *                 saved to this textview
     * @param calendar instance of calendar, date choosen by the user is assigned to this variable
     *
     *                 OPTIONAL: Set custom date Java format (e.g based on location)
     */

    private void datePicker(Context context, final TextView dateText, final Calendar calendar){

        DatePickerDialog datePicker = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, month);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                String date = new SimpleDateFormat("dd/MM/yyyy").format(calendar.getTime());
                dateText.setText(date);
            }
        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH) );

        datePicker.show();

    }


}
