package dominik.kedziak.sloth_profilescheduler.AccountSyncScheduler;

import android.app.IntentService;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import java.util.Objects;

import androidx.annotation.Nullable;
import dominik.kedziak.sloth_profilescheduler.Room.AlarmDatabase;
import dominik.kedziak.sloth_profilescheduler.Room.AlarmViewModel;
import dominik.kedziak.sloth_profilescheduler.Room.AlarmsDao;
import dominik.kedziak.sloth_profilescheduler.Room.Repository;
import dominik.kedziak.sloth_profilescheduler.Tools.NotificationHelper;

public class AccServiceOff extends IntentService {
    private static final String TAG = AccServiceOff.class.getSimpleName();

    public static PendingIntent getReminderPendingIntent(Context context, Uri uri) {
        Intent action = new Intent(context, AccServiceOff.class);
        action.setData(uri);
        return PendingIntent.getService(context, 0, action, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    public AccServiceOff() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

        long id = 0;
        if (intent != null) {
            id = Long.parseLong(Objects.requireNonNull(intent.getData()).toString());
        }

        AlarmViewModel mViewModel = new AlarmViewModel(getApplication());
        boolean isValid = mViewModel.isAlarmValid(id);

        if (intent != null && intent.getData() != null) {

            if (isValid) {
                Log.v(TAG, "alarm is valid!");
                ContentResolver.setMasterSyncAutomatically(false);
                Log.v(TAG, "Turning Acc off");

            } else {
                Log.v(TAG, "alarm is not valid");
            }

            NotificationHelper notificationHelper = new NotificationHelper();
            notificationHelper.createNotification(getApplicationContext(), id);
        }
    }
}

