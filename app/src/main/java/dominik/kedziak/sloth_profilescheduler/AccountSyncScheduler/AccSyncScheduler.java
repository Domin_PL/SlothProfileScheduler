package dominik.kedziak.sloth_profilescheduler.AccountSyncScheduler;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import dominik.kedziak.sloth_profilescheduler.Tools.AlarmManagerProvider;

public class AccSyncScheduler {


    public void setAlarmOff(Context context, long alarmTime, Uri reminderTask) {
        AlarmManager alarmManager = AlarmManagerProvider.getAlarmManager(context);
        Log.v("AlarmManager", "Initializing AM");
        PendingIntent operation =
                AccServiceOff.getReminderPendingIntent(context, reminderTask);

        if (Build.VERSION.SDK_INT >= 23) {
            assert alarmManager != null;
            alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, alarmTime, operation);
        } else {
            assert alarmManager != null;
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, alarmTime, operation);
        }
    }
    public void setAlarmOn(Context context, long alarmTime, Uri reminderTask) {
        AlarmManager alarmManager = AlarmManagerProvider.getAlarmManager(context);
        Log.v("AlarmManager", "Initializing AM");
        PendingIntent operation =
                AccServiceOn.getReminderPendingIntent(context, reminderTask);

        if (Build.VERSION.SDK_INT >= 23) {
            assert alarmManager != null;
            alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, alarmTime, operation);
        } else {
            assert alarmManager != null;
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, alarmTime, operation);
        }
    }

    public void setDailyAlarmOff(Context context, long alarmTime, Uri reminderTask, long repeatTime) {
        AlarmManager manager = AlarmManagerProvider.getAlarmManager(context);

        PendingIntent operation =
                AccServiceOff.getReminderPendingIntent(context, reminderTask);

        if (Build.VERSION.SDK_INT >= 23) {

            manager.setRepeating(AlarmManager.RTC_WAKEUP, alarmTime, repeatTime, operation);
        } else {
            manager.setRepeating(AlarmManager.RTC_WAKEUP, alarmTime,  repeatTime, operation);
        }

    }
    public void setDailyAlarmOn(Context context, long alarmTime, Uri reminderTask, long repeatTime) {
        AlarmManager manager = AlarmManagerProvider.getAlarmManager(context);

        PendingIntent operation =
                AccServiceOn.getReminderPendingIntent(context, reminderTask);

        if (Build.VERSION.SDK_INT >= 23) {

            manager.setRepeating(AlarmManager.RTC_WAKEUP, alarmTime, repeatTime, operation);
        } else {
            manager.setRepeating(AlarmManager.RTC_WAKEUP, alarmTime,  repeatTime, operation);
        }

    }



}
