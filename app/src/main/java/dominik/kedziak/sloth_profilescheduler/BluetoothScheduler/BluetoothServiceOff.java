package dominik.kedziak.sloth_profilescheduler.BluetoothScheduler;

import android.app.IntentService;
import android.app.PendingIntent;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import java.util.Objects;

import androidx.annotation.Nullable;
import dominik.kedziak.sloth_profilescheduler.Room.AlarmDatabase;
import dominik.kedziak.sloth_profilescheduler.Room.AlarmViewModel;
import dominik.kedziak.sloth_profilescheduler.Room.AlarmsDao;
import dominik.kedziak.sloth_profilescheduler.Tools.NotificationHelper;

public class BluetoothServiceOff extends IntentService {
    private static final String TAG = BluetoothServiceOff.class.getSimpleName();

    public static PendingIntent getReminderPendingIntent(Context context, Uri uri) {
        Intent action = new Intent(context, BluetoothServiceOff.class);
        action.setData(uri);
        return PendingIntent.getService(context, 0, action, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    public BluetoothServiceOff() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

        long id = 0;
        if (intent != null) {
            id = Long.parseLong(Objects.requireNonNull(intent.getData()).toString());
        }

        AlarmViewModel mViewModel = new AlarmViewModel(getApplication());
        boolean isValid = mViewModel.isAlarmValid(id);


        if (intent != null && intent.getData() != null) {
            if (isValid) {
                Log.v(TAG, "alarm is valid!");
                BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
                if (bluetoothAdapter.isEnabled()) {
                    bluetoothAdapter.disable();
                }

                NotificationHelper notificationHelper = new NotificationHelper();
                notificationHelper.createNotification(getApplicationContext(), id);
            } else {
                Log.v(TAG, "alarm is not valid");
            }

        }
    }
}