package dominik.kedziak.sloth_profilescheduler.BluetoothScheduler;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.net.Uri;
import android.os.Build;

import dominik.kedziak.sloth_profilescheduler.Tools.AlarmManagerProvider;

public class BluetoothScheduler {

    public void setAlarmOff(Context context, long alarmTime, Uri reminderTask) {
        AlarmManager alarmManager = AlarmManagerProvider.getAlarmManager(context);
        PendingIntent operation =
                BluetoothServiceOff.getReminderPendingIntent(context, reminderTask);

        if (Build.VERSION.SDK_INT >= 23) {
            alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, alarmTime, operation);
        } else {
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, alarmTime, operation);
        }
    }
    public void setAlarmOn(Context context, long alarmTime, Uri reminderTask) {
        AlarmManager alarmManager = AlarmManagerProvider.getAlarmManager(context);
        PendingIntent operation =
                BluetoothServiceOn.getReminderPendingIntent(context, reminderTask);

        if (Build.VERSION.SDK_INT >= 23) {
            alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, alarmTime, operation);
        } else {
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, alarmTime, operation);
        }
    }

    public void setDailyAlarmOn(Context context, long alarmTime, Uri reminderTask, long repeatTime) {
        AlarmManager manager = AlarmManagerProvider.getAlarmManager(context);

        PendingIntent operation =
                BluetoothServiceOn.getReminderPendingIntent(context, reminderTask);

        manager.setRepeating(AlarmManager.RTC_WAKEUP, alarmTime,  repeatTime, operation);

    }
    public void setDailyAlarmOff(Context context, long alarmTime, Uri reminderTask, long repeatTime) {
        AlarmManager manager = AlarmManagerProvider.getAlarmManager(context);

        PendingIntent operation =
                BluetoothServiceOff.getReminderPendingIntent(context, reminderTask);

        manager.setRepeating(AlarmManager.RTC_WAKEUP, alarmTime, repeatTime, operation);

    }

}
