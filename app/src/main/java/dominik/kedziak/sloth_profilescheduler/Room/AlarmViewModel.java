package dominik.kedziak.sloth_profilescheduler.Room;

import android.app.Application;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;


public class AlarmViewModel extends AndroidViewModel{

    private Repository mRepository;
    private LiveData<List<Alarms>> mAllAlarms;

    public AlarmViewModel(@NonNull Application application) {
        super(application);
        mRepository = new Repository(application);
        mAllAlarms = mRepository.getLiveData();
    }

    public LiveData<List<Alarms>> getallAlarms(){
        return mAllAlarms;
    }

    public boolean isAlarmValid(long id){
        return mRepository.isAlarmValid(id);
    }

    public int getCustomBrightnessValue(long id){
        return mRepository.getBrightness(id);
    }

    public int getProfile(long id){
        return mRepository.getProfile(id);
    }
}
