package dominik.kedziak.sloth_profilescheduler.Room

import android.app.Application
import android.util.Log
import android.view.KeyEvent
import androidx.lifecycle.LiveData
import kotlinx.coroutines.*

class Repository(val application: Application){

    var dao: AlarmsDao

    init {
        val db = AlarmDatabase.getInstance(application)
        dao = db.alarmsDao()
    }

    fun saveAlarm(alarms: Alarms): Long {
        return dao.saveAlarm(alarms)
    }

    fun isAlarmValid(id: Long): Boolean{
        return dao.isAlarmValid(id)
    }

    fun getBrightness(id: Long): Int {
        return dao.returnBrightness(id)
    }

    fun getLiveData(): LiveData<List<Alarms>> {
        return dao.readAlarms()
    }

    fun getValidAlarms(): List<Alarms>{
        return dao.validAlarms()
    }

    fun getProfile(id: Long): Int{
        return dao.returnProfile(id)
    }

}