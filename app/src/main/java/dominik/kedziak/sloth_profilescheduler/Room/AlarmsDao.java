package dominik.kedziak.sloth_profilescheduler.Room;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Index;
import androidx.room.Insert;
import androidx.room.Query;

@Dao
public interface AlarmsDao {

    @Insert
    long saveAlarm(Alarms alarms);

    @Query("DELETE FROM alarms WHERE _id = :id ")
    void deleteAlarm(long id);

    @Query("SELECT * FROM alarms ORDER BY _id ASC")
    LiveData<List<Alarms>> readAlarms();

    @Query("SELECT * FROM alarms WHERE valid = 1 ORDER BY _id ASC")
    List<Alarms> validAlarms();

    @Query("UPDATE alarms SET valid = :valid WHERE _id = :id")
    void validAlarm(final long id, final boolean valid);

    @Query("UPDATE alarms SET valid = :valid, title = :title, date = :mDate," +
            "time = :mTime, year = :year, month = :month, day = :day," +
            "hour = :hour, minute = :minute, valid = :valid, repeat = :repeat WHERE _id = :id")
    void updateAlarm(long id, String title, String mDate, String mTime, int year,
                     int month, int day, int hour, int minute, Boolean valid, String repeat);

    @Query("SELECT valid FROM alarms WHERE _id = :id")
    boolean isAlarmValid(long id);

    @Query("SELECT repeat FROM alarms WHERE _id = :id")
    String returnRepeatString(long id);

    @Query("SELECT profile FROM alarms WHERE _id = :id")
    int returnProfile(long id);

    @Query("UPDATE alarms SET valid = :valid WHERE _id = :id")
    void disableAlarm(long id, boolean valid);

    @Query("SELECT title FROM alarms WHERE _id = :id")
    String returnTitle(long id);

    @Query("SELECT count(*) FROM alarms")
    int returnCount();

    @Query("SELECT brightness FROM alarms WHERE _id = :id")
    int returnBrightness(long id);
}

