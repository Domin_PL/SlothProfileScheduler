package dominik.kedziak.sloth_profilescheduler.Room;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;

/**
 * This is database class
 */
@Database(entities = {Alarms.class}, version = 4)
public abstract class AlarmDatabase extends RoomDatabase {
    public abstract AlarmsDao alarmsDao();

private static volatile AlarmDatabase INSTANCE;

    /**
     * This is the most important part of the app.
     * It is responsible for migrating all current databases to Room
     * In the 3rd version there are few drops of table, so that we need
     * to create temporary tables.
     * See more here: https://www.sqlite.org/faq.html#q11
     * See changes in Room version of database:
     * https://gitlab.com/Domin_PL/SlothProfileScheduler/blob/Version2.2/README.md
     */


    private static final Migration MIGRATION_2_3 = new Migration(2, 3) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL("BEGIN TRANSACTION");
            database.execSQL("CREATE TEMPORARY TABLE alarm_table_backup(_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, title TEXT," +
                    " date TEXT, time TEXT, year INTEGER NOT NULL, month INTEGER NOT NULL, day INTEGER NOT NULL," +
                    " hour INTEGER NOT NULL," +
                    " minute INTEGER NOT NULL, repeat TEXT, profile INTEGER NOT NULL, valid INTEGER NOT NULL)");
            database.execSQL("UPDATE alarms SET profile = 0 WHERE profile IS NULL");
            database.execSQL("INSERT INTO alarm_table_backup SELECT _id, title, date, time, " +
                    "year, month, day, hour, minute, repeat, profile, valid FROM alarms");
            database.execSQL("DROP TABLE alarms");
            database.execSQL("CREATE TABLE alarms(_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, title TEXT, date TEXT, time TEXT, " +
                    "year INTEGER NOT NULL, month INTEGER NOT NULL, day INTEGER NOT NULL, hour INTEGER NOT NULL," +
                    " minute INTEGER NOT NULL, repeat TEXT, profile INTEGER NOT NULL," +
                    " valid INTEGER NOT NULL)");
            database.execSQL("INSERT INTO alarms SELECT _id, title, date, time, " +
                    "year, month, day, hour, minute, repeat, profile, valid FROM alarm_table_backup");
            database.execSQL("DROP TABLE alarm_table_backup");
            database.execSQL("COMMIT");
        }
    };

    private static final Migration MIGRATION_3_4 = new Migration(3,4) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL("BEGIN TRANSACTION");
            database.execSQL("ALTER TABLE alarms ADD brightness INTEGER NOT NULL DEFAULT -1");
            database.execSQL("COMMIT");
        }
    };

    public static AlarmDatabase getInstance(final Context context){
        if (INSTANCE == null){
            synchronized (AlarmDatabase.class){
                if (INSTANCE == null){
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                                AlarmDatabase.class, "alarmreminder.db")
                                .addMigrations( MIGRATION_2_3)
                                .addMigrations(MIGRATION_3_4)
                                .build();
                }
            }
        }
        return INSTANCE;
    }


}
