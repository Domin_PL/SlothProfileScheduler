package dominik.kedziak.sloth_profilescheduler.Room;

import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import static android.provider.BaseColumns._ID;

/*
 * Room entity for room database
 */


@Entity(tableName = "alarms")
public class Alarms {

    //set autoincrementing uniq id
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = _ID)
    private long _id;

    @ColumnInfo(name = "title")
    private String title;

    @ColumnInfo(name = "date")
    private String date;

    @ColumnInfo(name = "time")
    private String time;

    @ColumnInfo(name = "year")
    private int year;

    @ColumnInfo(name = "month")
    private int month;

    @ColumnInfo(name = "day")
    private int day;

    @ColumnInfo(name = "hour")
    private int hour;

    @ColumnInfo(name = "minute")
    private int minute;

    @ColumnInfo(name = "repeat")
    @Nullable
    private String repeat;

    @ColumnInfo(name = "profile")
    private int profile;

    @ColumnInfo(name = "valid")
    private boolean isValid;

    //primitive numbers like integers cannot be null, if not needed pass -1 by default
    @ColumnInfo(name = "brightness")
    private int brightness;

    public Alarms() {
    }

    /**
     * Public constructor
     * @param title uniq title of the alarm/task is passed into database
     * @param date date of the task
     * @param time time of the task including "PM/AM" when 12H clock is activated
     * @param year year of the task in calendar
     * @param month month of the task in calendar (it's between 0;11)
     * @param day day of the task in calendar
     * @param hour hourOfDay (24h format)
     * @param minute minute of task
     * @param repeat is alarm repeating or not, if not leave empty
     * @param profile profile of sound Task, see SharedPreferences Schema for more info
     * @param isValid is alarm valid or not
     */

    @Ignore
    public Alarms(String title, String date, String time,
                  int year, int month, int day, int hour, int minute,
                  @Nullable String repeat, int profile, boolean isValid) {
        this.title = title;
        this.date = date;
        this.time = time;
        this.year = year;
        this.month = month;
        this.day = day;
        this.hour = hour;
        this.minute = minute;
        this.repeat = repeat;
        this.profile = profile;
        this.isValid = isValid;
    }

    @Ignore
    public Alarms(String title,
                  String date,
                  String time,
                  int year,
                  int month,
                  int day,
                  int hour,
                  int minute,
                  @Nullable String repeat,
                  int profile,
                  boolean isValid,
                  int brightness) {

        this.title = title;
        this.date = date;
        this.time = time;
        this.year = year;
        this.month = month;
        this.day = day;
        this.hour = hour;
        this.minute = minute;
        this.repeat = repeat;
        this.profile = profile;
        this.isValid = isValid;
        this.brightness = brightness;
    }

    /**
     * setters of columns
     * @return getters
     */
    public long get_id() {
        return _id;
    }

    public void set_id(long _id) {
        this._id = _id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    @Nullable
    public String getRepeat() {
        return repeat;
    }

    public void setRepeat(@Nullable String repeat) {
        this.repeat = repeat;
    }

    public int getProfile() {
        return profile;
    }

    public void setProfile(int profile) {
        this.profile = profile;
    }

    public boolean isValid() {
        return isValid;
    }

    public void setValid(boolean valid) {
        isValid = valid;
    }

    public int getBrightness() {
        return brightness;
    }

    public void setBrightness(int brightness) {
        this.brightness = brightness;
    }
}
