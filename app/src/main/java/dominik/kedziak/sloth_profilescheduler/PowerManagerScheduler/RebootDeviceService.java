package dominik.kedziak.sloth_profilescheduler.PowerManagerScheduler;

import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import java.io.IOException;
import java.util.Objects;

import androidx.annotation.Nullable;
import dominik.kedziak.sloth_profilescheduler.Room.AlarmDatabase;
import dominik.kedziak.sloth_profilescheduler.Room.AlarmViewModel;
import dominik.kedziak.sloth_profilescheduler.Room.AlarmsDao;
import dominik.kedziak.sloth_profilescheduler.Tools.NotificationHelper;

public class RebootDeviceService extends IntentService {
    private static final String TAG = RebootDeviceService.class.getSimpleName();


    public static PendingIntent getReminderPendingIntent(Context context, Uri uri) {
        Intent action = new Intent(context, RebootDeviceService.class);
        action.setData(uri);
        return PendingIntent.getService(context, 0, action, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    public RebootDeviceService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        long id = 0;
        if (intent != null) {
            id = Long.parseLong(Objects.requireNonNull(intent.getData()).toString());
        }

        AlarmViewModel mViewModel = new AlarmViewModel(getApplication());
        boolean isValid = mViewModel.isAlarmValid(id);

        if (intent != null && intent.getData() != null) {
            if (isValid) {

                try {
                    Runtime.getRuntime().exec(new String[]{"/system/bin/su", "-c", "reboot now"});
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    Runtime.getRuntime().exec(new String[]{"/system/xbin/su", "-c", "reboot now"});
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    Runtime.getRuntime().exec(new String[]{"su", "-c", "reboot"});
                } catch (IOException e) {
                    e.printStackTrace();
                }

                NotificationHelper notificationHelper = new NotificationHelper();
                notificationHelper.createNotification(getApplicationContext(), id);

            } else {
                Log.v(TAG, "not valid");
            }
        }
    }
}
