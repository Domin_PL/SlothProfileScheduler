package dominik.kedziak.sloth_profilescheduler.PowerManagerScheduler;

import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.util.Log;

import java.io.IOException;
import java.util.Calendar;
import java.util.Objects;

import androidx.annotation.Nullable;
import dominik.kedziak.sloth_profilescheduler.R;
import dominik.kedziak.sloth_profilescheduler.Room.AlarmDatabase;
import dominik.kedziak.sloth_profilescheduler.Room.AlarmViewModel;
import dominik.kedziak.sloth_profilescheduler.Room.AlarmsDao;
import dominik.kedziak.sloth_profilescheduler.Tools.NotificationHelper;
import dominik.kedziak.sloth_profilescheduler.Tools.getDataRepeat;
import dominik.kedziak.sloth_profilescheduler.Tools.validationUpdater;

public class DeviceOffService extends IntentService {
    private static final String TAG = DeviceOffService.class.getSimpleName();


    public static PendingIntent getReminderPendingIntent(Context context, Uri uri) {
        Intent action = new Intent(context, DeviceOffService.class);
        action.setData(uri);
        return PendingIntent.getService(context, 0, action, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    public DeviceOffService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        long id = 0;
        if (intent != null) {
            id = Long.parseLong(Objects.requireNonNull(intent.getData()).toString());
        }

        AlarmViewModel mViewModel = new AlarmViewModel(getApplication());
        boolean isValid = mViewModel.isAlarmValid(id);

        if (intent != null && intent.getData() != null) {
                if (isValid) {
                    Process p = null;
                    try {
                        p = Runtime.getRuntime()
                                .exec(new String[]{"su", "-c", "reboot -p"});
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                 NotificationHelper notificationHelper = new NotificationHelper();
                 notificationHelper.createNotification(getApplicationContext(), id);
                } else {
                    Log.v(TAG, "not valid");
                }
            }
        }
    }