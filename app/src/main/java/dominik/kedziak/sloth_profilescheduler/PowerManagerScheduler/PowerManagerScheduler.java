package dominik.kedziak.sloth_profilescheduler.PowerManagerScheduler;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import dominik.kedziak.sloth_profilescheduler.Tools.AlarmManagerProvider;

public class PowerManagerScheduler {



    public void setAlarmOff(Context context, long alarmTime, Uri reminderTask) {
        AlarmManager alarmManager = AlarmManagerProvider.getAlarmManager(context);
        Log.v("AlarmManager", "Initializing AM");
        PendingIntent operation =
                DeviceOffService.getReminderPendingIntent(context, reminderTask);

        if (Build.VERSION.SDK_INT >= 23) {
            assert alarmManager != null;
            alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, alarmTime, operation);
        } else {
            assert alarmManager != null;
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, alarmTime, operation);
        }
    }
    public void setAlarmReboot(Context context, long alarmTime, Uri reminderTask) {
        AlarmManager alarmManager = AlarmManagerProvider.getAlarmManager(context);
        Log.v("AlarmManager", "Initializing AM");
        PendingIntent operation =
                RebootDeviceService.getReminderPendingIntent(context, reminderTask);

        if (Build.VERSION.SDK_INT >= 23) {
            assert alarmManager != null;
            alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, alarmTime, operation);
        } else {
            assert alarmManager != null;
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, alarmTime, operation);
        }
    }

    public void setAlarmOffDaily(Context context, long timeInMillis, Uri reminderTask, long intervalDayInMilis) {
        AlarmManager manager = AlarmManagerProvider.getAlarmManager(context);

        PendingIntent pendingIntent =
                DeviceOffService.getReminderPendingIntent(context, reminderTask);

        manager.setRepeating(AlarmManager.RTC_WAKEUP,timeInMillis,intervalDayInMilis,pendingIntent);
    }

    public void setAlarmRebootDaily(Context context, long timeInMillis, Uri reminderTask, long intervalDayInMilis) {
        AlarmManager manager = AlarmManagerProvider.getAlarmManager(context);

        PendingIntent pendingIntent =
                RebootDeviceService.getReminderPendingIntent(context, reminderTask);

        manager.setRepeating(AlarmManager.RTC_WAKEUP,timeInMillis,intervalDayInMilis,pendingIntent);
    }

}