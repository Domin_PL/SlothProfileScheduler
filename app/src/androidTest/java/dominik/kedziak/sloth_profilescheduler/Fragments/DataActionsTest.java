package dominik.kedziak.sloth_profilescheduler.Fragments;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import androidx.test.espresso.contrib.RecyclerViewActions;
import androidx.test.espresso.contrib.ViewPagerActions;
import androidx.test.espresso.intent.Intents;
import androidx.test.espresso.intent.matcher.IntentMatchers;
import androidx.test.rule.ActivityTestRule;
import dominik.kedziak.sloth_profilescheduler.ActivityEvents.AccSync;
import dominik.kedziak.sloth_profilescheduler.ActivityEvents.Bluetooth;
import dominik.kedziak.sloth_profilescheduler.ActivityEvents.Brightness;
import dominik.kedziak.sloth_profilescheduler.ActivityEvents.PowerManagerDeviceOff;
import dominik.kedziak.sloth_profilescheduler.ActivityEvents.PowerManagerReboot;
import dominik.kedziak.sloth_profilescheduler.ActivityEvents.SoundMode;
import dominik.kedziak.sloth_profilescheduler.ActivityEvents.WiFi;
import dominik.kedziak.sloth_profilescheduler.MainActivity;
import dominik.kedziak.sloth_profilescheduler.R;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.Matchers.allOf;

public class DataActionsTest {


    @Rule
    public ActivityTestRule<MainActivity> rule = new ActivityTestRule<>(MainActivity.class);


    @Before
    public void setUp(){
        onView(withId(R.id.pager))
                .perform(ViewPagerActions.scrollLeft());
        Intents.init();
    }

    @After
    public void tearDown(){
        Intents.release();
    }


    @Test
    public void launchWiFi() {
        onView(allOf(isDisplayed(), withId(R.id.recyclerview_main)))
                .perform(RecyclerViewActions.actionOnItemAtPosition(0, click()));
        Intents.intended(IntentMatchers.hasComponent(WiFi.class.getName()));
    }

    @Test
    public void launchBluetooth() {
        onView(allOf(isDisplayed(), withId(R.id.recyclerview_main)))
                .perform(RecyclerViewActions.actionOnItemAtPosition(1, click()));
        Intents.intended(IntentMatchers.hasComponent(Bluetooth.class.getName()));
    }

    @Test
    public void launchAccSync() {
        onView(allOf(isDisplayed(), withId(R.id.recyclerview_main)))
                .perform(RecyclerViewActions.actionOnItemAtPosition(2, click()));
        Intents.intended(IntentMatchers.hasComponent(AccSync.class.getName()));
    }

    @Test
    public void launchBrightness() {
        onView(allOf(isDisplayed(), withId(R.id.recyclerview_main)))
                .perform(RecyclerViewActions.actionOnItemAtPosition(3, click()));
        Intents.intended(IntentMatchers.hasComponent(Brightness.class.getName()));
    }

    @Test
    public void launchSoundMode() {
        onView(allOf(isDisplayed(), withId(R.id.recyclerview_main)))
                .perform(RecyclerViewActions.actionOnItemAtPosition(4, click()));
        Intents.intended(IntentMatchers.hasComponent(SoundMode.class.getName()));
    }

    @Test
    public void launchTurnOffDevice() {
        onView(allOf(isDisplayed(), withId(R.id.recyclerview_main)))
                .perform(RecyclerViewActions.actionOnItemAtPosition(5, click()));
        Intents.intended(IntentMatchers.hasComponent(PowerManagerDeviceOff.class.getName()));
    }

    @Test
    public void launchRebootDevice() {
        onView(allOf(isDisplayed(), withId(R.id.recyclerview_main)))
                .perform(RecyclerViewActions.actionOnItemAtPosition(6, click()));
        Intents.intended(IntentMatchers.hasComponent(PowerManagerReboot.class.getName()));
    }

}