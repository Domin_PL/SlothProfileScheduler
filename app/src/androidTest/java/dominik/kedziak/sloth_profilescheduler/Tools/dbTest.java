package dominik.kedziak.sloth_profilescheduler.Tools;

import android.content.Context;
import android.util.Log;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.util.List;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;


import androidx.room.Room;
import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import dominik.kedziak.sloth_profilescheduler.Room.AlarmDatabase;
import dominik.kedziak.sloth_profilescheduler.Room.Alarms;
import dominik.kedziak.sloth_profilescheduler.Room.AlarmsDao;

@RunWith(AndroidJUnit4.class)
public class dbTest {

    AlarmsDao mDao;
    AlarmDatabase mDb;

    @Before
    public void createDb(){
        Context context = ApplicationProvider.getApplicationContext();
        mDb = Room.inMemoryDatabaseBuilder(context, AlarmDatabase.class).build();
        mDao = mDb.alarmsDao();
    }

    @After
    public void closeDb() {
        mDb.close();
    }

    @Test
    public void testDb() {
        Alarms alarms = new Alarms(
                "Test alarm",
                "02/04/2019",
                "18:38",
                2019,
                3,
                2,
                18,
                38,
                null,
                0,
                true
        );
        mDao.saveAlarm(alarms);

        List<Alarms> alarmsList = mDao.validAlarms();
        Alarms testAlarm = alarmsList.get(0);
        Log.v("Alarms", alarmsList.toString());

        assertThat(testAlarm.get_id(), equalTo(1L));
    }


}
