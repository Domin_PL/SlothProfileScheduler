package dominik.kedziak.sloth_profilescheduler.Tools;

import android.app.Application;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Calendar;
import java.util.List;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import dominik.kedziak.sloth_profilescheduler.AccountSyncScheduler.AccSyncScheduler;
import dominik.kedziak.sloth_profilescheduler.BluetoothScheduler.BluetoothScheduler;
import dominik.kedziak.sloth_profilescheduler.BrightnessScheduler.BrightnessScheduler;
import dominik.kedziak.sloth_profilescheduler.PowerManagerScheduler.PowerManagerScheduler;
import dominik.kedziak.sloth_profilescheduler.R;
import dominik.kedziak.sloth_profilescheduler.Room.Alarms;
import dominik.kedziak.sloth_profilescheduler.Room.Repository;
import dominik.kedziak.sloth_profilescheduler.SoundModeScheduler.SoundScheduler;
import dominik.kedziak.sloth_profilescheduler.WiFiScheduler.WiFiScheduler;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

@RunWith(AndroidJUnit4.class)
public class onBootReceivedTest {

    private Context mContext;
    private Boolean updated = true;

    @Before
    public void setUp(){
        mContext = ApplicationProvider.getApplicationContext();
    }

    @Test
    public void onBootReceived(){
        final long intervalDayInMilis = 86400000;

        final String TAG = onBootReceived.class.getSimpleName();
        // it must be performed on the background thread

        Application application = (Application) mContext.getApplicationContext();
        final Repository mRepository = new Repository(application);



        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {

                //run algorithm for every item in the database
                List<Alarms> data = mRepository.getValidAlarms();
                for (int i = 0; i < data.size(); i++){         // O(n)
                    Alarms alarms = data.get(i);

                    //Uri is id of row in database, just parsed into Uri format
                    Uri uri = Uri.parse(String.valueOf(alarms.get_id()));

                    //if alarm is valid check if it should be turned off or not
                    if (alarms.isValid()){        // O(1)
                        //check if it is repeating alarm or not
                        if (alarms.getRepeat() != null && !alarms.getRepeat().equals("")) { // O(1)
                            //alarm is repeating, we can set it up
                            //get calendar instance
                            Calendar calendar = Calendar.getInstance();
                            //set up calendar
                            calendar.set(alarms.getYear(), alarms.getMonth(),
                                    alarms.getDay(), alarms.getHour(), alarms.getMinute());
                            //Schedule repeating alarm depending on alarm title
                            String services = alarms.getTitle();
                            if (services.equals(mContext.getString(R.string.wifiOff))) {
                                new WiFiScheduler().setAlarmOffDaily(mContext, calendar.getTimeInMillis(), uri,intervalDayInMilis);
                            } else if (services.equals(mContext.getString(R.string.wifiOn))) {
                                new WiFiScheduler().setAlarmOnDaily(mContext, calendar.getTimeInMillis(), uri,intervalDayInMilis);
                            } else if (services.equals(mContext.getString(R.string.loudMode))) {
                                new SoundScheduler().setAlarmLoudDaily(mContext, calendar.getTimeInMillis(), uri,intervalDayInMilis);
                            } else if (services.equals(mContext.getString(R.string.muteMode))) {
                                new SoundScheduler().setAlarmMuteDaily(mContext, calendar.getTimeInMillis(), uri,intervalDayInMilis);
                            } else if (services.equals(mContext.getString(R.string.disturbMode))) {
                                new SoundScheduler().setAlarmDisturbDaily(mContext, calendar.getTimeInMillis(), uri,intervalDayInMilis);
                            } else if (services.equals(mContext.getString(R.string.reboot))) {
                                new PowerManagerScheduler().setAlarmRebootDaily(mContext, calendar.getTimeInMillis(), uri,intervalDayInMilis);
                            } else if (services.equals(mContext.getString(R.string.turnOff))) {
                                new PowerManagerScheduler().setAlarmOffDaily(mContext, calendar.getTimeInMillis(), uri,intervalDayInMilis);
                            } else if (services.equals(mContext.getString(R.string.brightScreen))) {
                                new BrightnessScheduler().setDailyAlarmBright(mContext, calendar.getTimeInMillis(), uri,intervalDayInMilis);
                            } else if (services.equals(mContext.getString(R.string.darkScreen))) {
                                new BrightnessScheduler().setDailyAlarmDark(mContext, calendar.getTimeInMillis(), uri,intervalDayInMilis);
                            } else if (services.equals(mContext.getString(R.string.customScreen))) {
                                new BrightnessScheduler().setDailyAlarmCustom(mContext, calendar.getTimeInMillis(), uri,intervalDayInMilis);
                            } else if (services.equals(mContext.getString(R.string.btOn))) {
                                new BluetoothScheduler().setDailyAlarmOn(mContext, calendar.getTimeInMillis(), uri,intervalDayInMilis);
                            } else if (services.equals(mContext.getString(R.string.btOff))) {
                                new BluetoothScheduler().setDailyAlarmOff(mContext, calendar.getTimeInMillis(), uri, intervalDayInMilis);
                            }else if (services.equals(mContext.getString(R.string.accSyncOff))) {
                                new AccSyncScheduler().setDailyAlarmOff(mContext, calendar.getTimeInMillis(), uri, intervalDayInMilis);
                            } else if (services.equals(mContext.getString(R.string.accSyncOn))){
                                new AccSyncScheduler().setDailyAlarmOn(mContext, calendar.getTimeInMillis(), uri, intervalDayInMilis);
                            } else if(services.contains(mContext.getString(R.string.profil))) {
                                new SoundScheduler().setAlarmCustomDaily(mContext, calendar.getTimeInMillis(), uri,intervalDayInMilis);
                            }
                            else {
                                updated = false;
                                Log.v(TAG, "no alarm found");
                            }


                        } else if (alarms.getRepeat() != null && alarms.getRepeat().equals("")){
                            //alarm is one time only, we must check if it should be reactivated,
                            //or just scheduled one more time
                            //get calendar instance
                            Calendar calendar = Calendar.getInstance();
                            //get variable instances of date and time
                            int year = calendar.get(Calendar.YEAR);
                            int month = calendar.get(Calendar.MONTH);
                            int day = calendar.get(Calendar.DAY_OF_MONTH);
                            int hour = calendar.get(Calendar.HOUR_OF_DAY);
                            int minute = calendar.get(Calendar.MINUTE);

                            //check if alarm was scheduled on the actual year or year in future
                            if (alarms.getYear() >= year){
                                //check if alarm was scheduled in the same month or one in the future
                                //also must take in care if the alarm was scheduled in the future year,
                                //so that it will be always true
                                //in every else condition we will disable alarm
                                if (alarms.getMonth() >= month || alarms.getYear() > year){
                                    //check the day, same condition as above, next if statements
                                    //will work analogically
                                    if (alarms.getDay() >= day || alarms.getYear() > year){
                                        if (alarms.getHour() >= hour || alarms.getYear() > year){
                                            if (alarms.getMinute() >= minute || alarms.getYear() > year){
                                                //here we can set up alarm
                                                calendar.set(alarms.getYear(), alarms.getMonth(),
                                                        alarms.getDay(), alarms.getHour(), alarms.getMinute());

                                                //get title string
                                                String services = alarms.getTitle();
                                                if (services.equals(mContext.getString(R.string.wifiOff))) {
                                                    new WiFiScheduler().setAlarmOff(mContext, calendar.getTimeInMillis(), uri);
                                                } else if (services.equals(mContext.getString(R.string.wifiOn))) {
                                                    new WiFiScheduler().setAlarmOn(mContext, calendar.getTimeInMillis(), uri);
                                                } else if (services.equals(mContext.getString(R.string.loudMode))) {
                                                    new SoundScheduler().setAlarmLoud(mContext, calendar.getTimeInMillis(), uri);
                                                } else if (services.equals(mContext.getString(R.string.muteMode))) {
                                                    new SoundScheduler().setAlarmMute(mContext, calendar.getTimeInMillis(), uri);
                                                } else if (services.equals(mContext.getString(R.string.disturbMode))) {
                                                    new SoundScheduler().setAlarmDisturb(mContext, calendar.getTimeInMillis(), uri);
                                                } else if (services.equals(mContext.getString(R.string.reboot))) {
                                                    new PowerManagerScheduler().setAlarmReboot(mContext, calendar.getTimeInMillis(), uri);
                                                } else if (services.equals(mContext.getString(R.string.turnOff))) {
                                                    new PowerManagerScheduler().setAlarmOff(mContext, calendar.getTimeInMillis(), uri);
                                                } else if (services.equals(mContext.getString(R.string.brightScreen))) {
                                                    new BrightnessScheduler().setAlarmBright(mContext, calendar.getTimeInMillis(), uri);
                                                } else if (services.equals(mContext.getString(R.string.darkScreen))) {
                                                    new BrightnessScheduler().setAlarmDark(mContext, calendar.getTimeInMillis(), uri);
                                                } else if (services.equals(mContext.getString(R.string.customScreen))) {
                                                    new BrightnessScheduler().setAlarmCustom(mContext, calendar.getTimeInMillis(), uri);
                                                } else if (services.equals(mContext.getString(R.string.btOn))) {
                                                    new BluetoothScheduler().setAlarmOn(mContext, calendar.getTimeInMillis(), uri);
                                                } else if (services.equals(mContext.getString(R.string.btOff))) {
                                                    new BluetoothScheduler().setAlarmOff(mContext, calendar.getTimeInMillis(), uri);
                                                } else if (services.equals(mContext.getString(R.string.accSyncOff))) {
                                                    new AccSyncScheduler().setAlarmOff(mContext, calendar.getTimeInMillis(), uri);
                                                } else if (services.equals(mContext.getString(R.string.accSyncOn))){
                                                    new AccSyncScheduler().setAlarmOn(mContext, calendar.getTimeInMillis(), uri);
                                                } else if (services.contains(mContext.getString(R.string.profil))){
                                                    new SoundScheduler().setAlarmCustom(mContext, calendar.getTimeInMillis(), uri);
                                                }
                                                else {
                                                    Log.v(TAG, "no alarm found");
                                                    updated = false;
                                                }
                                            } else {
                                                validationUpdater validationUpdater = new validationUpdater();
                                                validationUpdater.disableAlarm(mContext, alarms.get_id());
                                            }
                                        }
                                    } else {
                                        validationUpdater validationUpdater = new validationUpdater();
                                        validationUpdater.disableAlarm(mContext, alarms.get_id());
                                    }
                                } else {
                                    validationUpdater validationUpdater = new validationUpdater();
                                    validationUpdater.disableAlarm(mContext, alarms.get_id());
                                }
                            } else {
                                validationUpdater validationUpdater = new validationUpdater();
                                validationUpdater.disableAlarm(mContext, alarms.get_id());
                            }

                        }
                    }
                }
            }
        });

        assertThat(updated, equalTo(true));
    }


}
