package dominik.kedziak.sloth_profilescheduler;

import android.content.Intent;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import androidx.test.espresso.contrib.DrawerActions;
import androidx.test.espresso.contrib.NavigationViewActions;
import androidx.test.espresso.intent.Intents;
import androidx.test.espresso.intent.matcher.IntentMatchers;
import androidx.test.rule.ActivityTestRule;
import dominik.kedziak.sloth_profilescheduler.Preferences.BrightnessSettings;
import dominik.kedziak.sloth_profilescheduler.Preferences.Preferences;
import dominik.kedziak.sloth_profilescheduler.Preferences.SoundSettings;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.contrib.DrawerMatchers.isOpen;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static org.junit.Assert.assertTrue;

public class MainActivityTest {



    @Rule
    public ActivityTestRule rule = new ActivityTestRule<>(MainActivity.class);

    @Before
    public void setUp(){
        onView(withId(R.id.drawer_layout))
                .perform(DrawerActions.open());
        onView(withId(R.id.drawer_layout)).check(matches(isOpen()));
        Intents.init();

    }

    @After
    public void tearDown(){
        Intents.release();
    }

    @Test
    public void checkOnSoundPreferencesClick_opensActivity(){
        onView(withId(R.id.nav_view)).perform(NavigationViewActions.navigateTo(R.id.soundSettings));
        Intents.intended(IntentMatchers.hasComponent(SoundSettings.class.getName()));
    }

    @Test
    public void checkOnBrightnessClick_opensActivity(){
        onView(withId(R.id.nav_view)).perform(NavigationViewActions.navigateTo(R.id.brightness));
        Intents.intended(IntentMatchers.hasComponent(BrightnessSettings.class.getName()));
    }

    @Test
    public void checkOnPreferencesClick_opensActivity(){
        onView(withId(R.id.nav_view)).perform(NavigationViewActions.navigateTo(R.id.preferences));
        Intents.intended(IntentMatchers.hasComponent(Preferences.class.getName()));
    }

    @Test
    public void checkOnRateClick_opensActivity(){
        onView(withId(R.id.nav_view)).perform(NavigationViewActions.navigateTo(R.id.rate));
        String action = Intents.getIntents().get(0).getAction();
        assert action != null;
        boolean isEqual = action.equals(Intent.ACTION_VIEW);
        assertTrue(isEqual);

    }

    @Test
    public void checkOnPermissionsClick(){
        onView(withId(R.id.nav_view)).perform(NavigationViewActions.navigateTo(R.id.permissions));
    }


}