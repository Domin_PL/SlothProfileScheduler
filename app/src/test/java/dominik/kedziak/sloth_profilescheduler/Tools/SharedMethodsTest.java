package dominik.kedziak.sloth_profilescheduler.Tools;

import android.content.Context;
import android.widget.ToggleButton;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import dominik.kedziak.sloth_profilescheduler.R;

import static org.junit.Assert.*;

public class SharedMethodsTest {

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Mock
    ToggleButton monday, tuesday, wednesday, thursday, friday, saturday, sunday;

    @Mock
    Context mContext;

    @Test
    public void releaseRepeat() {
        Mockito.when(mContext.getString(R.string.repeat_on)).thenReturn("Repeat on");
        Mockito.when(mContext.getString(R.string.monday)).thenReturn("Monday");
        Mockito.when(mContext.getString(R.string.tuesday)).thenReturn("Tuesday");
        Mockito.when(mContext.getString(R.string.wednesday)).thenReturn("Wednesday");
        Mockito.when(mContext.getString(R.string.thursday)).thenReturn("Thursday");
        Mockito.when(mContext.getString(R.string.friday)).thenReturn("Friday");
        Mockito.when(mContext.getString(R.string.saturday)).thenReturn("Saturday");
        Mockito.when(mContext.getString(R.string.sunday)).thenReturn("Sunday");
        Mockito.when(mContext.getString(R.string.repeat1Day)).thenReturn("Repeat daily");

        Mockito.when(monday.isChecked()).thenReturn(false);
        Mockito.when(tuesday.isChecked()).thenReturn(true);
        Mockito.when(wednesday.isChecked()).thenReturn(true);
        Mockito.when(thursday.isChecked()).thenReturn(true);
        Mockito.when(friday.isChecked()).thenReturn(true);
        Mockito.when(saturday.isChecked()).thenReturn(true);
        Mockito.when(sunday.isChecked()).thenReturn(false);

        StringBuilder builder = new StringBuilder();
        builder.append(mContext.getString(R.string.repeat_on)).append(" ");

        if (monday.isChecked() && tuesday.isChecked() && wednesday.isChecked()
        && thursday.isChecked() && friday.isChecked() && saturday.isChecked()
        && sunday.isChecked()){
            builder.setLength(0);
            builder = new StringBuilder();
            builder.append(mContext.getString(R.string.repeat1Day));
            assertEquals(mContext.getString(R.string.repeat1Day), builder.toString());
            return;
        }
        String semicolon = ", ";

        if (monday.isChecked()) builder.append(mContext.getString(R.string.monday)).append(semicolon);
        if (tuesday.isChecked()) builder.append(mContext.getString(R.string.tuesday)).append(semicolon);
        if (wednesday.isChecked()) builder.append(mContext.getString(R.string.wednesday)).append(semicolon);
        if (thursday.isChecked()) builder.append(mContext.getString(R.string.thursday)).append(semicolon);
        if (friday.isChecked()) builder.append(mContext.getString(R.string.friday)).append(semicolon);
        if (saturday.isChecked()) builder.append(mContext.getString(R.string.saturday)).append(semicolon);
        if (sunday.isChecked()) builder.append(mContext.getString(R.string.sunday));

        int length = builder.length() - 2;
        char lastChar = builder.charAt(length);
        if (lastChar == ',') builder.setLength(length);

        StringBuilder mBuilder = new StringBuilder();
        mBuilder.append(mContext.getString(R.string.repeat_on)).append(" ")
//                .append(" ").append(mContext.getString(R.string.monday))
                .append(mContext.getString(R.string.tuesday)).append(semicolon)
                .append(mContext.getString(R.string.wednesday)).append(semicolon)
                .append(mContext.getString(R.string.thursday)).append(semicolon)
                .append(mContext.getString(R.string.friday)).append(semicolon)
                .append(mContext.getString(R.string.saturday));
//                .append(", ").append(mContext.getString(R.string.sunday));
        System.out.println(mBuilder.toString());

        assertEquals(mBuilder.toString(), builder.toString());

    }
}