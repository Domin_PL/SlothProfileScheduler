package dominik.kedziak.sloth_profilescheduler.ActivityEvents;

import org.junit.Test;

import static org.junit.Assert.*;

public class BaseClassTest {


                 //Conversion//
// Calendar.hour_of_day  // 24h format //  12h format //
//    0 0 12 AM
//    1 1 1 AM
//    2 2 2 AM
//    3 3 3 AM
//    4 4 4 AM
//    5 5 5 AM
//    6 6 6 AM
//    7 7 7 AM
//    8 8 8 AM
//    9 9 9 AM
//    10 10 10 AM
//    11 11 11 AM
//    12 12 12 PM
//    13 13 1 PM
//    14 14 2 PM
//    15 15 3 PM
//    16 16 4 PM
//    17 17 5 PM
//    18 18 6 PM
//    19 19 7 PM
//    20 20 8 PM
//    21 21 9 PM
//    22 22 10 PM
//    23 23 11 PM


    /**
     * Test uses JUnit to test
     * @param hour from range 0-23
     * @param minute from range 0-60
     * @param is24H boolean, if true uses 24h clock format, if false converts to 12h clock format
     * @return String with date and time after conversion
     */
    private String updateTime(int hour, int minute, boolean is24H) {
        if (hour > 24 || hour < 0 ||  minute > 60 || minute < 0) return null;
        //midnight is 12 AM ||  noon  is 12PM
        String time;
        int mHour = hour;
        if (!is24H && hour > 12 && hour != 24) {
            hour -= 12;
        }

        if (hour == 0 && !is24H) hour = 12;

        if (minute < 10) {
            time = hour + ":" + "0" + minute;
        } else {
            time  = hour  + ":" + minute;
        }

        // If 12h time format add a proper information
        if (!is24H){
            time =  (mHour <= 11)  ? time + " AM" : time + " PM";
        }

        return time;
    }

    @Test
    public void checkTimeConversion(){
        assertEquals("12:00 AM", updateTime(0,0,false));
        assertEquals("0:00", updateTime(0,0,true));
        assertEquals("12:00 PM", updateTime(12, 0,false));
        assertEquals("12:00", updateTime(12, 0, true));
        assertEquals("12:50 PM", updateTime(12, 50, false));
        assertEquals("12:50", updateTime(12, 50, true));
        assertEquals("11:50 PM", updateTime(23, 50, false));

    }



    // Month in java // Current month  // range 0 - 11
    // 0  1  January etc.
    // 1  2
    // 2  3
    // 3  4
    // 4  5
    // 5  6
    // 6  7
    // 7  8
    // 8  9
    // 9  10
    //10  11
    //11  12
    private String updateDate(int day, int month, int year) {
        if (day < 1|| day > 31 || month < 0 || month > 11 || year < 0) return null;
        String mDay = "";
        mDay = String.valueOf(day);
        String date;
        if (day < 10){
            mDay = "0" + day;
        }
        month +=  1;

        date = (month < 10) ? mDay + "/0" + month + "/" + year
                : mDay + "/" + month + "/" + year;
        return date;
    }

    @Test
    public void checkDate(){
        assertEquals("01/01/2000", updateDate(1,0, 2000));
        assertNull(updateDate(0, 12, 2000));
        assertEquals("31/03/2000", updateDate(31,2,2000));
    }


}