package dominik.kedziak.sloth_profilescheduler.ActivityEvents;

import org.junit.Test;
import org.mockito.Mock;

import static org.junit.Assert.assertEquals;

public class EnumsTest {

    @Mock
    Enums enums = new Enums();

    @Test
    public void getSPKey() {
        String outcome = enums.getSPKey(Enums.AlarmEnum.IS24hCLOCK);
        assertEquals("is24hClock", outcome);
    }

}